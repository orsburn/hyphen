<?php

function nl_langinfo(int $item) : bool
{
	$localItems = localeconv();

	return $localItems[$item] ?? false;
}