<?php

namespace Hyphen;

trait Welcome
{
	/**
	 * Write a welcome message.
	 *
	 * @param string $message
	 * @param string $environment Options are:  cli, command, console, browser, js, javascript, log.
	 *
	 * @return string|null  Returns null if none of the allowed environment options are provided.
	 */
	public function welcome(string $message = null, string $environment = "log") : ?string
	{
		switch(strtolower($environment))
		{
			case "cli":
			case "command":
			case "console":
				return $this->welcomeCli($message);
			case "browser":
			case "js":
			case "javascript":
				return $this->welcomeJs($message);
			case "log":
				return $this->welcomeLog($message);
		}

		return null;
	}

	/**
	 * Writes a welcome message to standard out.
	 *
	 * @param string $message
	 * @return string
	 */
	public function welcomeCli(string $message = null) : string
	{
		echo $this->welcomeLogo() . PHP_EOL . ($message ?? "");

		return $this->welcomeLogo() . PHP_EOL . ($message ?? "");
	}

	/**
	 * Writes a welcome message to the JavaScript console in the browser.
	 *
	 * @param string $message
	 * @return string
	 */
	public function welcomeJs(string $message = null) : string
	{
		echo "\n";
		echo "<script>\n";
		echo "console.log(\"" . str_replace(PHP_EOL, "\n", $this->welcomeLogo()) . "\n\");\n";
		echo "console.log(\"" . ($message ?? "") . "\");\n";
		echo "</script>\n";

		return $this->welcomeLogo() . "\n" . ($message ?? "");
	}

	/**
	 * Writes a welcome message to the current Hyphen::logFile, or returns the welcome message to the caller.
	 *
	 * @param string $message
	 * @param bool $noLog
	 * @return string
	 */
	public function welcomeLog(string $message = null, bool $noLog = false) : string
	{

		if($noLog)
		{
			return $this->welcomeLogo() . PHP_EOL . ($message ?? "");
		}

 		/*
		We're using a new instance of Logger\Log() since the log file location may be
		re-assigned after initialization.
		*/
		$logger = new Logger\Log($this->logFile);

		return $logger->log($this->welcomeLogo() . PHP_EOL . ($message ?? ""), $logger::MESSAGE);
	}

	public function welcomeLogo() : string
	{
		$logo  = PHP_EOL;
		$logo .=    ".: ||      || :." . PHP_EOL;
		$logo .=  ".::: ||      || :::." . PHP_EOL;
		$logo .= ".:::: ||      || ::::." . PHP_EOL;
		$logo .= "::::: || %%%% || :::::" . PHP_EOL;
		$logo .= "':::: ||      || ::::'" . PHP_EOL;
		$logo .=  "'::: ||      || :::'" . PHP_EOL;
		$logo .=    "': ||      || :'" . PHP_EOL;
		$logo .= PHP_EOL;
		$logo .= "   H  Y  P  H  E  N" . PHP_EOL;
		$logo .= PHP_EOL;

		return $logo;
	}
}