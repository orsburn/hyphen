<?php

namespace Hyphen\Http;

use Hyphen\Arrays;
use Hyphen\SerializeXml;

/*
FIXME: This library is currently undergoing a transformation from an attempt at
	   an HTTP request and response API, to a routing library. While the routing
	   is currently in a usable state, there is no support for PSR-7 response.
	   Instead, the response content is generated from the route handler defined
	   in the route file, and this library sends the content down to the client.
	   As the library matures, support for PSR-7 responses will be implemented.
*/

/**
 * Simple HTTP routing library.
 *
 * A suite of "routes" are defined through one of a number of method calls that correspond with the various HTTP method types (GET, POT, PUT, et cetera).
 */
class Router
{
	use ResponseTraits\SendApplication;
	use ResponseTraits\SendAudio;
	use ResponseTraits\SendBinary;
	use ResponseTraits\SendData;
	use ResponseTraits\SendDocument;
	use ResponseTraits\SendImage;
	use ResponseTraits\SendVideo;

	const MULTIPLE_INSTANCES_WARNING_MESSAGE = "An instance of " . __CLASS__ . " has already been instantiated. There can only be one instance for an application. Multiple instances may lead to unpredictable behavior";

	                                          public int $statusCode = 404;
	                                          public string $contentType = "html";
	                                          public ?string $contentFilename;
	                                          public string $request; // This is the whole request, the path info and the query string.
	                                          public string $requestMethod;
	                                          public ?string $requestContentType;
	                                          public ?string $pathInfo;
	/** @var null|array<string>            */ public ?array $pathInfoParsed;
	                                          public ?string $queryString;
	/** @var null|array<string>            */ public ?array $queryStringParsed;
	                                          public string $characterSet = "UTF-8";
	/** @var null|\stdClass                */ public ?object $variables;
	/** @var null|\stdClass                */ public ?object $parameters;
	/** @var null|string|\SimpleXMLElement */ public $payload;
	                                          public null|string|object $rawPayload;
	/** @var \stdClass                     */ public object $accessControl;
	                                          public bool $ignoreGetPayloads = false;
	                                          public string $sourceDirectory = "/src";
	/** @var array<string>                 */ public array $headers = [];
	/** @var array<string>                 */ public array $xHeaders = ["X-Powered-By"=>"Hyphen"];
	                                          public bool $useXHeaders = true;
	                                          public bool $allowListings = true; // This is available to support service discovery. (Supported types are HTML, JSON, and XML.)
	/** @var array<string>                 */ public array $statusCodeTemplates = [];

	/** @var string                        */ private string $tokenPattern   = "[\\w\\d\\s\\-\\+\\.\\,\\^\\~\\!\\?\\@\\#\\\\$\\%\\*\\(\\)]";
	//                                                                                                                  `--`----------------- This series of backslashes is necessary in order to properly include the dollar sign in the sequence.
	                                          private bool $handledRequest = false;
	                                          private object $register;
	                                          private string $defaultDataContentType = "json";
	                                          private bool $showListing = false; // Used to determine if the routing table should be displayed.

	public function __construct()
	{
		if(\Hyphen\Types\Type::exists(__CLASS__))
		{
			trigger_error(self::MULTIPLE_INSTANCES_WARNING_MESSAGE, E_USER_WARNING);
		}

		$this->register = new \stdClass();
		$this->requestMethod = strtoupper(filter_input(INPUT_SERVER, "REQUEST_METHOD"));
		$this->requestContentType = $_SERVER["CONTENT_TYPE"] ?? null;
		$this->parsePathInfo();
		$this->parseQueryString();
		$this->request = trim("$this->pathInfo?$this->queryString", "?");
		$this->accessControl = new \stdClass();
		$this->accessControl->allowOrigin = null; // "*";
		$this->accessControl->allowMethods = ["GET", "POST", "PUT", "OPTIONS"]; // May be specified as a string as well. DELETE is not allowed by default.
		$this->accessControl->allowCredentials = null;
		$this->accessControl->allowHeaders = null;
		$this->accessControl->exposeHeaders = null;
		$this->accessControl->maxAge = 5;

		$this->statusCodeTemplates[0]   = __DIR__ . "/../Templates/Http/StatusCodes/unhandled.html";
		$this->statusCodeTemplates[404] = __DIR__ . "/../Templates/Http/StatusCodes/404.html";
		$this->statusCodeTemplates[405] = __DIR__ . "/../Templates/Http/StatusCodes/405.html";
		$this->statusCodeTemplates[500] = __DIR__ . "/../Templates/Http/StatusCodes/500.html";
	}

	/**
	 * @param array|object|callable $function
	 *
	 * @return mixed
	 */
	public function get(string $resource, $function)
	{
		return $this->handleRequest("GET", $resource, $function);
	}

	/*
	TODO: There should be a check that disallows requests that provide headers
	      $_SERVER(HTTP_*) that are not "approved" in
	      $this->accessControl->allowHeaders.
	*/

	/**
	 * @param array|object|callable $function
	 */
	public function put(string $resource, $function) : mixed
	{
		return $this->handleRequest("PUT", $resource, $function);
	}


	/**
	 * @param array|object|callable $function
	 */
	public function post(string $resource, $function) : mixed
	{
		return $this->handleRequest("POST", $resource, $function);
	}


	/**
	 * @param array|object|callable $function
	 */
	public function delete(string $resource, $function) : mixed
	{
		return $this->handleRequest("DELETE", $resource, $function);
	}


	/**
	 * @param array|object|callable $function
	 */
	public function patch(string $resource, $function) : mixed
	{
		return $this->handleRequest("PATCH", $resource, $function);
	}

	public function options() : void
	{
		$this->statusCode
			= (array_search("OPTIONS", $this->normalizeList($this->accessControl->allowMethods)) !== false) && // 1.  Ensures we should even answer an OPTIONS request.
			  (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]) !== false) &&                             // 2.  Ensures a request method is provided.
			  $this->searchRequestInRegister($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"], $this->pathInfo)  // 3.  Searches for a matching resource against the request method.
			? 204
			: 405; // Method not allowed

		/*
		FIXME: There needs to be some more work here concerning whether or not the request is a CORS request or not.

		       https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS
		       https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Request-Method
		*/
		$this->headers["Allow"] = implode(", ", $this->normalizeList($this->accessControl->allowMethods));
		http_response_code($this->statusCode);
		$this->sendHeaders();
	}

	/**
	 * Report the routes table to the client.
	 *
	 * @param string|null $sortBy The column to sort the table by (name, method, or endpoint)
	 * @param string|null $sortDirection The direction the sort by column will be listed (up or down)
	 * @param null|array|string|null $methods Limit the table to only these methods
	 * @return array
	 */
	public function list(?string $sortBy = "endpoint", ?string $sortDirection = "up", null|array|string $methods = null) : array
	{
		if(!is_null($methods))
		{
			$methods
				= is_array($methods)
				? array_map(function($element){return strtoupper($element);}, $methods)
				: array_map(function($element){return strtoupper(trim($element));}, explode(",", $methods));
		}

		$methods
			= $methods
			? array_intersect($this->accessControl->allowMethods, $methods)  // Just the methods in common.
			: $this->accessControl->allowMethods;

		sort($methods); // Sorting the list of allowed methods allows them to be reported in alphabetical order per endpoint.

		$list = [];

		// Repeating for each method.
		foreach($methods as $method)
		{
			if(!isset($this->register->{$method}))
			{
				continue;
			}

			$method = strtoupper($method);

			foreach($this->register->{$method} as $endpoint)
			{
				$foundInList = array_search($endpoint["endpoint"], array_column($list, "endpoint"));

				$name
					= !is_callable($endpoint["options"]) && isset($endpoint["options"]["name"])
					? $endpoint["options"]["name"]
					: null;

				$list[] = [
					"endpoint"=>$endpoint["endpoint"],
					"method"=>$method,
					"name"=>$name
				];
			}
		}

		Arrays::sortGrid($list, "endpoint");

		$finalList = [];
		$lastEntry = ["endpoint"=>null, "method"=>null, "name"=>null];

		foreach($list as $entry)
		{
			// No matching endpoint.
			if($entry["endpoint"] !== $lastEntry["endpoint"])
			{
				$finalList[] = $entry;
			}
			// Matching endpoint, so checking the name too.
			elseif($entry["name"] === $lastEntry["name"])
			{
				$finalList[count($finalList) - 1]["method"] .= ", " . $entry["method"];
			}
			// Names don't match either.
			else
			{
				$finalList[] = $entry;
			}

			$lastEntry = $entry;
		}

		Arrays::sortGrid(
			$finalList,
			$sortBy,
			$sortDirection == "down" ? (Arrays::SORT_DOWN) : Arrays::SORT_UP
		);

		return $finalList;
	}

	private function parsePathInfo() : void
	{
		$this->pathInfo
			= $_SERVER["PATH_INFO"]
			? rtrim($_SERVER["PATH_INFO"], "/")
			: null;

		if($this->pathInfo)
		{
			$this->pathInfoParsed = explode("/", ltrim($this->pathInfo, "/"));
		}

		if
		(
			!is_null($this->pathInfoParsed) &&
			(count($this->pathInfoParsed) == 1) &&
			($this->pathInfoParsed[0] === "")
		)
		{
			$this->pathInfoParsed = null;
		}
	}

	private function parseQueryString() : void
	{
		$this->queryString
			= isset($_SERVER["QUERY_STRING"]) && $_SERVER["QUERY_STRING"] !== ""
			? $_SERVER["QUERY_STRING"]
			: null;

		if(count($_GET) === 0){$this->queryStringParsed = null;}
		else{$this->queryStringParsed = $_GET;}

		$this->parameters = new \stdClass();

		foreach($_GET as $key=>$value)
		{
			$this->parameters->{$key} = $value;
		}
	}

	/**
	 * Compares the URI to the route.
	 *
	 * If the two match, then certain request items are set.  For instance, the payload, variables, and the query parameters.
	 *
	 * @param string $resource The route to compare the URI against.
	 *
	 * @return boolean Returns true if the route matches the URI, or false otherwise.
	 */
	private function matchesResource(string $resource) : bool
	{
		$resourceRegEx = preg_replace("/{" . $this->tokenPattern . "*}/", "($this->tokenPattern*)", $this->escapeResource($resource));

		// This tries to pull the variables out of the path info.
		preg_match("/$resourceRegEx/", ($this->pathInfo ?? ""), $matches);

		/*
		The following conditional works because if a request is submitted with a path
		info of just "/" it is removed entirely.  So, it is then considered just an
		empty string.  So, the pathInfo property will also be an empty string.

			NOTE: Some servers, like Apache, will intercept this situation and issue a
			      redirect (301 Moved Permanently) to the same URI with a trailing slash

		If there are variables in the resource, then they would have been matched in the
		preg_match() call above.  In this case, $matches[0] is used to verify that the
		resource has been matched.

		It should also be noted that the ugliness here is appreciated.
		*/
		if(
			(
				($resource == $this->pathInfo) ||               // This is trying to accommodate "root-type" resources.
				(($resource == "/") && ($this->pathInfo == "")) // Either as an empty string or just slash (/).
			) ||
			(
				(count($matches) > 0) &&
				($this->pathInfo == $matches[0]) // Element zero is the request itself.
			)
		)
		{
			array_shift($matches);
			$this->populate($matches, $resource);                             // The rest "elements" ( {something} ) are resource variables.
			$this->payload                                                    // Allow payloads on GET requests to be ignored if so desired.
				= !!$this->ignoreGetPayloads && $this->requestMethod == "GET" // It's an easy accommodation.  The default is to allow them.
				? null                                                        //
				: (file_get_contents("php://input") ?: null);                 //

			$this->rawPayload = $this->payload;

			switch($this->requestContentType)
			{
				case "application/json":
					$this->payload = json_decode($this->payload ?? "");
					break;
				case "application/xml":
					$this->payload = simplexml_load_string($this->payload ?? "") ?: null;
					if(is_null($this->payload))
					{
						$this->payload = new \SimpleXMLElement("");
					}
					break;
			}

			return true;
		}

		return false;
	}

	/**
	 * Pulls all the variable names from the rest path and applies their corresponding values from the URI.
	 *
	 * @param array $values The values from the URI that will be applied to their matching REST path variable names.
	 * @param string $resource The rest path that matches the URI.
	 * @return void Nothing is returned.  Instead the variables property is populated.
	 */
	private function populate(array $values, string $resource) : void
	{
		preg_match_all("/{($this->tokenPattern*)}/", $resource, $matches);

		$variableNames = $matches[1];

		if(count($values) == count($variableNames))
		{
			$this->variables = new \stdClass();

			for($i = 0; $i < count($values); $i ++)
			{
				$this->variables->{$variableNames[$i]} = $values[$i];
			}
		}
	}


	private function escapeResource(string $resource) : string
	{
		return str_replace(
			[  "/",   "?",   "&",   ".",   ",",   "^",   "~",   "@",   "#",   "%",   "*",   "(",   ")"],
			["\\/", "\\?", "\\&", "\\.", "\\,", "\\^", "\\~", "\\@", "\\#", "\\%", "\\*", "\\(", "\\)"],
			$resource);
	}

	/**
	 * @param array|object|callable|function $function
	 */
	private function handleRequest(string $resourceMethod, string $resource, $function) : mixed
	{
		$this->showListing                                           // Determines if a routing table can be displayed or not
			= $this->allowListings && isset($this->parameters->list) // If the routing table is to be shown, then no request
			? true                                                   // handling needs to be done. This is why the flag is
			: false;                                                 // being checked in the rest of the method.

		// ------------------------------------------------------- Registering the route
		$this->register->{$resourceMethod} = $this->register->{$resourceMethod} ?? [];

		if(count($this->searchRegister($resourceMethod, $resource)) > 0)
		{
			trigger_error("Hyphen\\Http\\Response:  A URI that resembles this one was already declared:  $resourceMethod $resource.  This may include an empty resource which will be considered as \"/\"", E_USER_WARNING);
		}

		$this->register->{$resourceMethod}[] = ["endpoint"=>$resource, "options"=>$function];

		// ----------------------------------------------- Checking if method is allowed
		if(!$this->showListing &&
			array_search(
				strtoupper($this->requestMethod),
				$this->normalizeList($this->accessControl->allowMethods)
			)
			=== false
		)
		{
			http_response_code($this->statusCode = 405); // Method Not Allowed
			$this->send();
			$this->handledRequest = true;
			// FIXME: Should this exit or return? I'm thinking return.
			exit; // HTTP methods don't match, so we're bailing.
		}
		// ---------------------------------- There is a route and the method is allowed
		elseif(!$this->showListing && ($this->requestMethod == $resourceMethod) && $this->matchesResource($resource))
		{
			$this->statusCode = 200; // OK

			ob_start();                                          // The handler function may generate content, so it's
			$returnValue = $this->runRequestFunction($function); // captured here.
			$content = ob_get_contents();                        //
			ob_end_clean();                                      //

			http_response_code($this->statusCode); // Remember that the status code could be changed in the handler code.

			$this->sendHeaders();

			/*
			If the status code is not 200, and there is a template for whatever the status
			code is, the content will be overwritten with the status code template content.
			*/
			if($this->statusCode != 200)
			{
				$content = $this->sendStatusCodeTemplate();
			}

			/*
			The request handler is going to either output content or return a value that is
			the content.  It doesn't make any sense to do both.  This function favors
			content that is output from the handler, and fails over to returned content.
			*/
			if(strlen($content ?: "") > 0)
			{
				$this->send($content);
			}
			else
			{
				$this->send($returnValue);
			}

			$this->handledRequest = true;

			return $returnValue; // Returning whatever comes back from the request handler in case the caller needs it for anything.
		}

		return null;
	}

	/**
	 * @param array<mixed>|object|callable|function $function
	 */
	private function runRequestFunction($function) : mixed // INFO:  This is returned as mixed since the result of call_user_func_array() can't be known ahead of time.
	{
		$data = new \stdClass();

		$data->variables = $this->variables;
		$data->parameters = $this->parameters;
		$data->payload = $this->payload;
		$data->rawPayload = $this->rawPayload;

		// If we have an anonymous function, just call it and be done!
		if(is_callable($function))
		{
			return call_user_func_array($function, [$data]);
		}

		$namespace = "";
		$class = "";
		$functionName = "";
		$arguments = [];

		// ----------------------------------------------------------- arguments
		if(is_array($function) && isset($function["arguments"]))
		{
			$arguments = $function["arguments"];
		}
		elseif(is_object($function) && isset($function->arguments))
		{
			$arguments = $function->arguments;
		}

		$arguments[] = $data;

		// ----------------------------------------------------------- namespace
		if(is_array($function) && isset($function["namespace"]))
		{
			$namespace = $function["namespace"];
		}
		elseif(is_object($function) && isset($function->namespace))
		{
			$namespace = $function->namespace;
		}

		$namespace = rtrim($namespace, "\\") . "\\";

		// --------------------------------------------------------------- class
		if(is_array($function) && isset($function["class"]))
		{
			$class = $function["class"];
		}
		elseif(is_object($function) && isset($function->class))
		{
			$class = $function->class;
		}

		$class = rtrim($class, "\\()");

		// ------------------------------------------------------------ function
		if(is_array($function) && isset($function["function"]))
		{
			$functionName = $function["function"];
		}
		elseif(is_object($function) && isset($function->function))
		{
			$functionName = $function->function;
		}

		// ---------------------------------------------------------- load class
		if($pathToClass = $this->getClassFilePath($class))
		{
			if(file_exists($pathToClass))
			{
				include_once $pathToClass;
			}
		}

		// ------------------------------------------------------ class instance
		$classInstance
			= $class !== ""
			? new $class()
			: false;

		// -------------------------------------------------------- run function
		if($classInstance && method_exists($classInstance, $functionName))
		{
			return call_user_func_array(array($classInstance, $functionName), $arguments);
		}
		elseif(function_exists($namespace . $functionName))
		{
			$fun = $namespace . $functionName;

			return call_user_func_array($fun, $arguments);
		}
	}

	private function send(mixed $content = null) : void
	{
		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types

		switch
		(
			  (                                                // Trying to automatically determine the best format to output
				(is_array($content) || is_object($content)) && // the content with.  If the content is an array or an object
				(strtolower($this->contentType) == "html")     // and the content type is HTML, then the output will default
			  )                                                // to JSON formatted data.
			? $this->defaultDataContentType                    //
			: strtolower($this->contentType)                   //
		)
		{
			// ------------------------------------------------------------------------ Data
			case "ics": $this->sendIcs($content); break;
			case "json": $this->sendJson($content); break;
			case "xml": $this->sendXml($content); break;

			// -------------------------------------------------------------------- Document
			case "css": $this->sendCss($content); break;
			case "csv": $this->sendCsv($content); break;
			case "doc": $this->sendDoc($content); break;
			case "docx": $this->sendDocx($content); break;
			case "eot": $this->sendEot($content); break;
			case "epub": $this->sendEpub($content); break;
			case "excelcsv": $this->sendExcelCsv($content); break;
			case "html": $this->sendHtml($content); break;
			case "odp": $this->sendOdp($content); break;
			case "ods": $this->sendOds($content); break;
			case "odt": $this->sendOdt($content); break;
			case "pdf": $this->sendPdf($content); break;
			case "plain":
			case "ppt": $this->sendPpt($content); break;
			case "pptx": $this->sendPptx($content); break;
			case "raw": $this->sendText($content); break;
			case "rtf": $this->sendRtf($content); break;
			case "txt":
			case "text":
			case "txt": $this->sendText($content); break;
			case "woff": $this->sendWoff($content); break;
			case "woff2": $this->sendWoff2($content); break;
			case "xhtml": $this->sendXhtml($content); break;
			case "xls": $this->sendXls($content); break;
			case "xlsx": $this->sendXlsx($content); break;

			// ----------------------------------------------------------------- Application
			case "jar": $this->sendJar($content); break;
			case "javascript":
			case "js": $this->sendJavaScript($content); break;

			// ----------------------------------------------------------------------- Image
			case "bitmap":
			case "bmp": $this->sendGif($content); break;
			case "gif": $this->sendGif($content); break;
			case "jpeg":
			case "jpg": $this->sendJpeg($content); break;
			case "png": $this->sendPng($content); break;
			case "svg": $this->sendSvg($content); break;
			case "webp": $this->sendWebP($content); break;

			// ---------------------------------------------------------------------- Binary
			case "7z": $this->send7Z($content); break;
			case "bz": $this->sendBz($content); break;
			case "bz2": $this->sendBz2($content); break;
			case "binary": $this->sendBinary($content); break;
			case "rar": $this->sendRar($content); break;
			case "zip": $this->sendZip($content); break;

			// ----------------------------------------------------------------------- Music
			case "mp3": $this->sendMp3($content); break;
			case "weba": $this->sendWebA($content); break;

			// ----------------------------------------------------------------------- Video
			case "mpeg": $this->sendMpeg($content); break;
			case "webm": $this->sendWebM($content); break;

			default:
				if(is_array($content) || is_object($content))
				{
					$contentOut = json_encode($content) ?: "";
					header("Content-Type: application/json;charset=$this->characterSet");
					header("Content-Length: " . strlen($contentOut));
					echo $contentOut;
				}
				else
				{
					header("Content-Type: text/plain;charset=$this->characterSet");
					header("Content-Length: " . strlen($content ?? ""));
					echo $content;
				}
		}
	}

	/**
	 * Sends the response headers except for Content-Type and Content-Length.  Those headers are actually sent in the send() method.
	 */
	private function sendHeaders() : void
	{
		/*
		FIXME: The allowed methods will be sent with either an Allow header or an
		       Access-Control-Allow-Methods header.  It will depend on a CORS request,
		       or not.  (In response to request with Origin header.)

		       This wrangling should probably be done in the Response->option() method.
		*/
		foreach($this->headers as $header=>$value)
		{
			header("$header: $value");
		}

		if(isset($this->accessControl->allowOrigin) && ($this->accessControl->allowOrigin !== ""))
		{
			$check = implode(", ", $this->normalizeList($this->accessControl->allowOrigin));

			header("Access-Control-Allow-Origin: " . $check);

			if($check != "*")
			{
				header("Vary: Origin");
			}
		}

		// FIXME: See the note above about sending either this header, or the Allow header.
		if(isset($this->accessControl->allowMethods) && ($this->accessControl->allowMethods != ""))
		{
			header("Access-Control-Allow-Methods: " . implode(", ", $this->normalizeList($this->accessControl->allowMethods)));
		}

		if(isset($this->accessControl->allowCredentials) && ($this->accessControl->allowMethods != ""))
		{
			$check
				= $this->accessControl->allowCredentials
				? "true"
				: "false";

			header("Access-Control-Allow-Credentials: $check");
		}

		if(isset($this->accessControl->allowHeaders) && ($this->accessControl->allowHeaders != ""))
		{
			header("Access-Control-Allow-Headers: " . implode(", ", $this->normalizeList($this->accessControl->allowHeaders)));
		}

		if(isset($this->accessControl->exposeHeaders) && ($this->accessControl->exposeHeaders != ""))
		{
			header("Access-Control-Expose-Headers: " . implode(", ", $this->normalizeList($this->accessControl->exposeHeaders)));
		}

		if(isset($this->accessControl->maxAge) && ($this->accessControl->maxAge !== ""))
		{
			header("Access-Control-Max-Age: " . (string) $this->accessControl->maxAge);
		}

		if($this->useXHeaders)
		{
			foreach($this->xHeaders as $header=>$value)
			{
				header("$header: $value");
			}
		}
	}

	/**
	 * Accepts a collection of items, whether as an array or a string of entries, and returns them as an array.
	 *
	 * @param string|array<string> $list
	 */
	private function normalizeList(string|array $list) : array
	{
		return
			is_array($list)
				? array_map(function($entry) : string{return $entry;}, $list)
				: array_map(function($entry) : string{return trim($entry);}, explode(",", $list));
	}

	private function searchRegister(string $resourceMethod, string $resource) : array
	{
		$resourceMethod = trim(strtoupper($resourceMethod));

		if(isset($this->register->{$resourceMethod}) === false)
		{
			return [];
		}

		$resource
			= ($resource == "")
			? "/"
			: $resource;

		$tokenPatternForHits =
			str_replace
			(
				array("[", "]"),
				array("[\\{", "\\}]"),
				$this->tokenPattern
			);

		$regExMaskForHits =                         // This "mask" is so that we can look for something in the form
			preg_replace                            // of /path/{something} and match it to anything that looks like
			(                                       // that.  We will need a regex like /path/([{CHARACTERS_HERE}]).
				"/{" . $this->tokenPattern . "*}/", // This is pseudo regex, but the idea is that expression needs
				"($tokenPatternForHits*)",          // to match {characters} (including the curly braces).  The
				$this->escapeResource($resource)    // parentheses provides for all matches.
			);                                      //

		$routes = array_column($this->register->{$resourceMethod}, "endpoint");

		return preg_grep("/^$regExMaskForHits$/", $routes);
	}

	/**
	 * Looks for a request in the register for a method.
	 *
	 *     For example, try to find POST /sample/1234 in the post register of resources.
	 *
	 * @param string $resourceMethod  This is the name of the register that should be search in.
	 * @param null|string $pathInfo  The request that is being matched.
	 *
	 * @return mixed If a match is found, then the matched resource is returned.  <code>void</code> will be returned otherwise.
	 */
	private function searchRequestInRegister(string $resourceMethod, ?string $pathInfo)
	{
		/*
		Notice how when the register entries are modified to have a pattern in place of
		the variable placeholders, an entry with just a variable will match almost any
		path info.

		Path Info       Register Entry       Register Entry with Pattern
		------------    -----------------    ---------------------------------------------------
		/sample/1234    /sample/{testing}    \/sample\/([\w\d\s\-\+\.\,\^\~\!\?\@\#\$\%\*\(\)]*)
		/sample/1234    /{testing}           \/([\w\d\s\-\+\.\,\^\~\!\?\@\#\$\%\*\(\)]*)

		The search expression needs to search from the beginning of the path info all
		the way to the end.  When the expression is applied it needs to look like:
		/^EXPRESSION$/.
		*/
		$resourceMethod = strtoupper($resourceMethod);

		if(isset($this->register->{$resourceMethod}) === false)
		{
			return;
		}

		/*
		This re-assignment is happening in order to accommodate a path that is an empty
		string or doesn't exist at all.
		*/
		$pathInfo
			= ($pathInfo == "") || ($pathInfo == null)
			? "/"
			: $pathInfo;

		foreach($this->register->{$resourceMethod} as $registerEntry) // FIXME: I don't think this dynamic reference will work anymore.
		//                       `---------------`-- Is this notation still allowed?
		{
			$registerEntryRegEx = preg_replace(
				"/{" . $this->tokenPattern . "*}/",
				"($this->tokenPattern*)",
				$this->escapeResource($registerEntry["endpoint"])
			);

			if(preg_match("/^$registerEntryRegEx$/", $pathInfo))
			{
				return $registerEntry;
			}
		}
	}

	private function getClassFilePath(string $class) : ?string
	{
		if($class == "")
		{
			return null;
		}

		$class = str_replace("\\", "/", $class);
		$checkDirectory = "/" . ltrim($this->sourceDirectory, "/"); // Initializing the check directory by conditioning the source directory first.
		$checkDirectory                                             // That is, ensuring a leading slash.  The default directory is /src and does
			= $checkDirectory == "/"                                // not need to be unset if the directory is indeed /src.  The search will just
			? ""                                                    // start one directory deeper if index.php is also in /src/index.php instead of
			: $checkDirectory;                                      // /index.php.
		$checkDirectory = str_replace("\\", "/", dirname(filter_input(INPUT_SERVER, "SCRIPT_FILENAME"))) . $checkDirectory;

		while
		(
			($checkDirectory != "/") &&
			($checkDirectory != "\\") &&
			(preg_match("/^.{1}\:[\\\\|\/]$/", $checkDirectory) === 0)
		)
		{
			if(file_exists("$checkDirectory/$class.php"))
			{
				return "$checkDirectory/$class.php";
			}

			$checkDirectory = dirname($checkDirectory);
		}

		throw new \Exception("The class $class() assigned in the $this->requestMethod $this->pathInfo route could not be found.");
	}

	private function sendStatusCodeTemplate() : string
	{
		if(file_exists($this->statusCodeTemplates[$this->statusCode] ?? ""))
		{
			$content = file_get_contents($this->statusCodeTemplates[$this->statusCode]) ?: "";
		}
		else
		{
			$content = file_get_contents($this->statusCodeTemplates[0]) ?: ""; // The unhandled status code template
		}

		switch($this->statusCode)
		{
			case 404:
			case 500:
				$content = str_replace("[hyphen:requestedPage]", filter_input(INPUT_SERVER, "REQUEST_URI"), $content);
				break;
			case 405:
				$content = str_replace("[hyphen:requestMethod]", $this->requestMethod, $content);
				break;
			default:
				$content = str_replace("[hyphen:statusCode]", $this->statusCode, $content);
		}

		return $content;
	}

	public function __destruct()
	{
		/*
		There are a collection of things that can only happen once all the registration
		calls have been made (Response->get(), Response->post(), et cetera). An example
		of this is outputting the routing table (Response->showListing). Without running
		through all the registration calls, this table would not be able to be
		completed. That is what this function is doing.
		*/
		if($this->showListing)
		{
			$listSort = $this->parameters->listSort ?? null;           // The column the report should be sorted on (name | method | endpoint [default])
			$listDirection = $this->parameters->listDirection ?? null; // The direction the sort column should be report in (up | down)
			$listMethods = $this->parameters->listMethods ?? null;     // Limit the report to only these methods
			$this->contentType = $this->parameters->listFormat ?? "json";

			// It's in the browser, and there is either no list format specified, or the
			// contentType is already set to HTML from above.
			if(PHP_SAPI != "cli" && (!isset($this->parameters->listFormat) || ($this->contentType == "html")))
			{
				$this->contentType = "html";

				$endpointsTable = $this->list($listSort, $listDirection, $listMethods);

				$endpointsTableRow = file_get_contents(__DIR__ . "/../Templates/Http/endpointsTableRow.html") ?: "";
				$endpointsTableRowOutput = "";  // Note the subtle difference here with the "s" character.
				$endpointsTableRowsOutput = ""; //

				foreach($endpointsTable as $details)
				{
					$endpointsTableRowOutput = str_replace(
						"[hyphen:endpointsTableRow_name]",
						$details["name"],
						$endpointsTableRow);

					$endpointsTableRowOutput = str_replace(
						"[hyphen:endpointsTableRow_method]",
						$details["method"],
						$endpointsTableRowOutput);

					$endpointsTableRowOutput = str_replace(
						"[hyphen:endpointsTableRow_endpoint]",
						$details["endpoint"],
						$endpointsTableRowOutput);

					$endpointsTableRowsOutput .= $endpointsTableRowOutput . "\n";
				}

				$this->send(
					str_replace(
						"[hyphen:endpointsTableRows]",
						trim($endpointsTableRowsOutput),
						file_get_contents(__DIR__ . "/../Templates/Http/endpointsTable.html") ?: ""
					)
				);
			}
		}
		elseif($this->requestMethod == "OPTIONS")
		{
			$this->options();
		}
		elseif($this->handledRequest == false)
		{
			$this->contentType = "html";
			http_response_code($this->statusCode = 404); // Not Found
			$this->send();
		}
	}
}