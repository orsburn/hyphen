<?php

namespace Hyphen\Http\ResponseTraits;

trait SendBinary
{
	/**
	 * @param mixed $binary
	 */
	private function send7Z($binary) : void
	{
		header("Content-Type: application/x-7z-compressed");
		echo $binary;
	}

	/**
	 * @param mixed $binary
	 */
	private function sendBinary($binary) : void
	{
		header("Content-Type: application/octet-stream");
		echo $binary;
	}

	/**
	 * @param mixed $binary
	 */
	private function sendBz($binary) : void
	{
		header("Content-Type: application/x-bzip");
		echo $binary;
	}

	/**
	 * @param mixed $binary
	 */
	private function sendBz2($binary) : void
	{
		header("Content-Type: application/x-bzip2");
		echo $binary;
	}

	/**
	 * @param mixed $binary
	 */
	private function sendMpkg($binary) : void
	{
		header("Content-Type: application/vnd.apple.installer+xml");
		echo $binary;
	}

	/**
	 * @param mixed $binary
	 */
	private function sendRar($binary) : void
	{
		header("Content-Type: application/x-rar-compressed");
		echo $binary;
	}

	/**
	 * @param mixed $binary
	 */
	private function sendZip($binary) : void
	{
		header("Content-Type: application/zip");
		echo $binary;
	}
}