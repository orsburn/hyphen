<?php

namespace Hyphen\Http\ResponseTraits;

trait SendImage
{
	/**
	 * @param mixed $image
	 */
	private function sendBitmap($image) : void
	{
		header("Content-Type: image/jpg");
		echo $image;
	}

	/**
	 * @param mixed $image
	 */
	private function sendGif($image) : void
	{
		header("Content-Type: image/gif");
		echo $image;
	}

	/**
	 * @param mixed $image
	 */
	private function sendIco($image) : void
	{
		header("Content-Type: image/vnd.microsoft.icon");
		echo $image;
	}

	/**
	 * @param mixed $image
	 */
	private function sendJpeg($image) : void
	{
		header("Content-Type: image/jpg");
		echo $image;
	}

	/**
	 * @param mixed $image
	 */
	private function sendPng($image) : void
	{
		header("Content-Type: image/png");
		echo $image;
	}

	/**
	 * @param mixed $image
	 */
	private function sendSvg($image) : void
	{
		header("Content-Type: image/svg+xml");
		echo $image;
	}

	/**
	 * @param mixed $image
	 */
	private function sendWebP($image) : void
	{
		header("Content-Type: image/webp");
		echo $image;
	}
}