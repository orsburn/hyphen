<?php

namespace Hyphen\Http\ResponseTraits;

use Hyphen\SerializeXml;

trait SendData
{
	/**
	 * @param mixed $data
	 */
	private function sendIcs($data) : void
	{
		header("Content-Type: text/calendar");
		echo $data;
	}

	/**
	 * @param mixed $data
	 */
	private function sendJson($data) : void
	{
		header("Content-Type: application/json;charset=$this->characterSet");

		// Try to serialize the content if it's an array or an object.
		if(is_array($data) || is_object($data))
		{
			echo json_encode($data);
		}
		else
		{
			echo $data;
		}
	}

	/**
	 * @param mixed $data
	 */
	private function sendXml($data) : void
	{
		header("Content-Type: application/xml;charset=$this->characterSet");

		// Try to serialize the content if it's an array or an object.
		if(is_array($data) || is_object($data))
		{
			echo SerializeXml::toXmlDocument(["listing"=>$data]);
		}
		else
		{
			echo $data;
		}
	}
}