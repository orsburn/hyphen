<?php

namespace Hyphen\Http\ResponseTraits;

trait SendAudio
{
	/**
	 * @param mixed $audio
	 */
	private function sendMp3($audio) : void
	{
		header("Content-Type: audio/mpeg");
		echo $audio;
	}

	/**
	 * @param mixed $audio
	 */
	private function sendWebA($audio) : void
	{
		header("Content-Type: audio/webm");
		echo $audio;
	}
}