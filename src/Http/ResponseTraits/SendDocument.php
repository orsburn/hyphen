<?php

namespace Hyphen\Http\ResponseTraits;

trait SendDocument
{
	/**
	 * @param mixed $document
	 */
	private function sendCss($document) : void
	{
		header("Content-Type: text/css;charset=$this->characterSet");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendCsv($document) : void
	{
		header("Content-Disposition: inline; filename=\"" . ($this->contentFilename ?? "CSV Data.csv") . "\"");
		header("Content-Type: text/csv"); // header("Content-type: application/vnd.ms-excel");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendDoc($document) : void
	{
		header("Content-Type: application/msword");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendDocx($document) : void
	{
		header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendEot($document) : void
	{
		header("Content-Type: application/vnd.ms-fontobject");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendEpub($document) : void
	{
		header("Content-Type: application/epub+zip");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendExcelCsv($document) : void
	{
		header("Content-Disposition: inline; filename=\"" . ($this->contentFilename ?? "Excel CSV Data.csv") . "\"");
		header("Content-Type: application/vnd.ms-excel");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendHtml($document) : void
	{
		header("Content-Type: text/html;charset=$this->characterSet");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendOdp($document) : void
	{
		header("Content-Type: application/vnd.oasis.opendocument.presentation");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendOds($document) : void
	{
		header("Content-Type: application/vnd.oasis.opendocument.spreadsheet");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendOdt($document) : void
	{
		header("Content-Type: application/vnd.oasis.opendocument.text");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendPdf($document) : void
	{
		header("Content-Type: application/pdf");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendPpt($document) : void
	{
		header("Content-Type: application/vnd.ms-powerpoint");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendPptx($document) : void
	{
		header("Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendRtf($document) : void
	{
		header("Content-Type: application/rtf");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendText($document) : void
	{
		header("Content-Type: text/plain;charset=$this->characterSet");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendWoff($document) : void
	{
		header("Content-Type: font/woff");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendWoff2($document) : void
	{
		header("Content-Type: font/woff2");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendXhtml($document) : void
	{
		header("Content-Type: application/xhtml+xml");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendXls($document) : void
	{
		header("Content-Type: application/vnd.ms-excel");
		echo $document;
	}

	/**
	 * @param mixed $document
	 */
	private function sendXlsx($document) : void
	{
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		echo $document;
	}
}