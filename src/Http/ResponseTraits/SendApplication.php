<?php

namespace Hyphen\Http\ResponseTraits;

trait SendApplication
{
	/**
	 * @param mixed $application
	 */
	private function sendJar($application) : void
	{
		header("Content-Type: application/java-archive");
		echo $application;
	}

	/**
	 * @param mixed $application
	 */
	private function sendJavaScript($application) : void
	{
		header("Content-Type: text/javascript;charset=$this->characterSet");
		echo $application;
	}
}