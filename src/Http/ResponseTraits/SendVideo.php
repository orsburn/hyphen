<?php

namespace Hyphen\Http\ResponseTraits;

trait SendVideo
{
	/**
	 * @param mixed $video
	 */
	private function sendMpeg($video) : void
	{
		header("Content-Type: video/mpeg");
		echo $video;
	}

	/**
	 * @param mixed $video
	 */
	private function sendWebM($video) : void
	{
		header("Content-Type: video/webm");
		echo $video;
	}
}