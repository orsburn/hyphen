<?php

namespace Hyphen\Math;

class Geometry
{
	public static function circleArea(float $radius, bool $diameter = false) : float
	{
		return
			  $diameter
			? M_PI * (($radius / 2) * ($radius / 2)) // Radius is passed as diameter.
			: M_PI * ($radius * $radius);
	}

	public static function circumference(float $radius, bool $diameter = false) : float
	{
		return
			  $diameter
			? M_PI * $radius // Radius is passed as diameter.
			: M_PI * (2 * $radius);
	}

	public static function diagonal(float $a, float $b) : float
	{
		return sqrt(($a * $a) + ($b * $b));
	}

	public static function parallelogramArea(float $base, float $height) : float
	{
		return self::rectangleArea($base, $height);
	}

	public static function parallelogramPerimeter(float $a, float $b) : float
	{
		return self::rectanglePerimeter($a, $b);
	}

	public static function rectangleArea(float $base, float $height) : float
	{
		return $base * $height;
	}

	public static function rectanglePerimeter(float $a, float $b) : float
	{
		return $a + $a + $b + $b;
	}

	public static function triangleArea(float $base, float $height) : float
	{
		return ($base * $height) / 2;
	}
}