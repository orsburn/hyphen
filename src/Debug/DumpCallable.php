<?php

namespace Hyphen\Debug;

trait DumpCallable
{
	/**
	 * Dump a callable as HTML.
	 *
	 * @return string
	 */
	private static function htmlCallable() : string
	{
		$dumpId = self::dumpId();

		$output  = "<!-- Callable dump identified by -->\n";
		$output .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-callable.css") . "\n</style>\n\n";
		$output .= "<table class=\"callable\" id=\"callable_$dumpId\"><tbody><tr><th class=\"type\">callable</th><td>Closure</td></tr></tbody></table>\n";
		$output .= "<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump a callable to the CLI.
	 *
	 * @param bool $formatted
	 * @return string
	 */
	private static function cliCallable(bool $formatted) : string
	{
		return
			  $formatted
			? CliColors::callable("callable") . " " . CliColors::typeCallable("Closure")
			: "Closure";
	}
}