<?php

namespace Hyphen\Debug;

trait DumpBoolean
{
	/**
	 * Dump a boolean as HTML.
	 *
	 * @param bool $check
	 * @return string
	 */
	private static function htmlBoolean(bool $check) : string
	{
		$dumpId = self::dumpId();

		$output  = "<!-- Boolean dump identified by -->\n";
		$output .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-boolean.css") . "\n</style>\n\n";
		$output .= "<table class=\"boolean\" id=\"boolean_$dumpId\"><tbody><tr><th class=\"type\">boolean</th><td>" . (!!$check ? "true" : "false") . "</td></tr></tbody></table>\n";
		$output .= "<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump boolean to the CLI.
	 *
	 * @param bool $check
	 * @param bool $formatted
	 * @return string
	 */
	private static function cliBoolean(bool $check, bool $formatted) : string
	{
		$value = (!!$check ? "true" : "false");

		$output =
			  $formatted
			? CliColors::typeBool("bool") . " " . CliColors::bool($value)
			: "bool $value";

		return $output;
	}
}