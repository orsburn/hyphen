<?php

namespace Hyphen\Debug;

trait DumpArray
{
	/**
	 * Dumps an array as HTML.
	 *
	 * @param array $check
	 * @return string
	 */
	private static function htmlArray(array $check) : string
	{
		$dumpId        = self::dumpId();
		$elementNumber = 0;

		$html  = "<!-- Array dump identified by $dumpId ........................... -->\n";
		$html .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-array.css") . "\n</style>\n\n";

		$html .= "<table class=\"array\" id=\"a_$dumpId\">\n";
		$html .= "	<thead class=\"arrayHead\" id=\"a_$dumpId-head\">\n";
		$html .= "		<tr>\n";
		$html .= "			<th id=\"a_$dumpId\" colspan=\"2\">array (" . count($check) . ")</th>\n";
		$html .= "		</tr>\n";
		$html .= "	</thead>\n";
		$html .= "	<tbody id=\"a_$dumpId-body\">\n";

		$js  = "<script>\n";
		$js .= "var a_showHideElement_$dumpId = " . file_get_contents(__DIR__ . "/support/dump-element.js") . "\n\n";
		$js .= "var a_showHideBody_$dumpId = " . file_get_contents(__DIR__ . "/support/dump-body.js") . "\n\n";

		$js .= "document.querySelector('#a_$dumpId-head').addEventListener('click', a_showHideBody_$dumpId);\n\n";

		foreach($check as $key => $value)
		{
			ob_start();                         // Buffering for whatever the type is for the current
			self::dump($value);                 // value.
			$currentValue = ob_get_contents();  //
			ob_end_clean();                     //

			$html .= "		<tr id=\"a_$dumpId" . "_$elementNumber\">\n";
			$html .= "			<th id=\"a_$dumpId" . "_$elementNumber-key\">$key</th>\n";
			$html .= "			<td id=\"a_$dumpId" . "_$elementNumber-value\">$currentValue</td>\n";
			$html .= "		<tr>\n";

			$js .= "document.querySelector('#a_$dumpId" . "_$elementNumber-key').addEventListener('click', a_showHideElement_$dumpId);\n";

			$elementNumber ++;
		}

		$output  = "$html\t</tbody>\n";
		$output .= "</table>";
		$output .= "\n\n";
		$output .= "$js</script>";
		$output .= "\n<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dumps an array to the CLI.
	 *
	 * @param array $check
	 * @param bool $formatted
	 * @param int $nestLevel
	 * @return string
	 */
	private static function cliArray(array $check, bool $formatted, int $nestLevel) : string
	{
		// This is assumed to be 0 on the very first invocation, so it's bumped-up here
		// to make sure the next indents if it's ever called again.
		$nestLevel ++;
		$keys = array_keys($check);

		$keyLength = array_reduce(                             // The key length is the width of the
			$keys,                                             // array key "label."  This is important
			function($carry, $current){                        // for outputting the key below.
				return max($carry, strlen((string) $current)); //
			},                                                 //
			0                                                  //
		);                                                     //

		$output
			= $formatted
			? "array (" . count($check) . ")" . PHP_EOL
			: CliColors::typeArray("array") . " (" . count($check) . ")" . PHP_EOL;

		foreach($check as $key => $value)
		{
			$renderedKey
				= $formatted
				? $key
				: CliColors::arrayKey($key);

			// -------------------------------------------------------- Rendering the indent
			for($i = 0; $i < $nestLevel; $i ++) // Handling the level of nesting in cases
			{                                   // where this has been called recursively.
				$output .= self::$cliIndent;    //
			}                                   //

			// ----------------------------------------------------- Rendering the key/index
			$output .= str_pad(                                       // This is here because the SPL str_pad() function
				$renderedKey,                                         // doesn't properly handle padding strings with
				($keyLength + (strlen($renderedKey) - strlen($key))), // control characters in them.
				" ",                                                  //
				STR_PAD_LEFT                                          //
			);                                                        //

			// --------------------------------------------------------- Rendering the value
			$output .= " | " . self::dump($value, true, $formatted, $nestLevel) . PHP_EOL;
		}

		return $output;
	}
}