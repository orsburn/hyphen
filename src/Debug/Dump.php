<?php

namespace Hyphen\Debug;

class Dump
{
	use DumpArray;
	use DumpBoolean;
	use DumpCallable;
	use DumpError;
	use DumpFloat;
	use DumpInteger;
	use DumpNull;
	use DumpObject;
	use DumpString;
	use DumpTable;

	/** @var string */ private static $cliIndent = "    ";

	/**
	 * Dumps content.
	 *
	 * @param mixed $check
	 * @param boolean $return
	 * @param bool $formatted This tells the function to format the output.  It is only relevant for command-line usage, and is ignored otherwise.
	 * @return string|null
	 */
	public static function dump($check, bool $return = false, bool $formatted = true) : ?string
	{
		$nestLevel = 0;

		if(func_num_args() > 3)
		{
			$nestLevel = func_get_args()[3];
		}

		$output = "";

		if(PHP_SAPI != "cli")
		{
			if(is_callable($check))
			{
				return self::sendOutput(self::htmlCallable(), $return);
			}

			switch(gettype($check))
			{
				case "boolean": $output = self::htmlBoolean($check); break;
				case "integer": $output = self::htmlInteger($check); break;
				case "double": $output = self::htmlFloat($check); break;
				case "string": $output = self::htmlString($check); break;
				case "array": $output = self::htmlArray($check); break;
				case "object": $output = self::htmlObject($check); break;
				// case "resource": $output = self::htmlResource($check, $return); break; // FIXME  Unimplemented
				// case "resource (closed)": $output = self::htmlClosedResource($check, $return); break; // FIXME  Unimplemented
				case "NULL": $output = self::htmlNull(); break;
				// case "unknown type": $output = self::htmlUnknown($check, $return); break; // FIXME  Unimplemented
			}
		}
		else
		{
			if(is_callable($check))
			{
				return self::sendOutput(self::cliCallable($formatted), $return);
			}

			switch(gettype($check))
			{
				case "boolean": $output = self::cliBoolean($check, $formatted); break;
				case "integer": $output = self::cliInteger($check, $formatted); break;
				case "double": $output = self::cliFloat($check, $formatted); break;
				case "string": $output = self::cliString($check, $formatted); break;
				case "array": $output = self::cliArray($check, $formatted, $nestLevel); break;
				case "object": $output = self::cliObject($check, $formatted); break;
				// case "resource": $output = self::cliResource($check, $return); break; // FIXME  Unimplemented
				// case "resource (closed)": $output .= self::cliClosedResource($check, $return); break; // FIXME  Unimplemented
				case "NULL": $output = self::cliNull($formatted); break;
				// case "unknown type": $output = self::cliUnknown($check, $return); break; // FIXME  Unimplemented
			}
		}

		return self::sendOutput($output, $return);
	}

	private static function dumpId() : string
	{
		$pool = "abcdefghijklmnopqrstuvwxyz01234567890";

		$id  = $pool[random_int(0, 35)];
		$id .= $pool[random_int(0, 35)];
		$id .= $pool[random_int(0, 35)];
		$id .= $pool[random_int(0, 35)];
		$id .= $pool[random_int(0, 35)];
		$id .= $pool[random_int(0, 35)];
		$id .= $pool[random_int(0, 35)];

		return $id . $pool[random_int(0, 35)];
	}

	/**
	 * Sends the content to the consumer.
	 *
	 * It can either display the content directly, or return it as a value to be consumer by some other process.
	 *
	 * @param string $output The output to be rendered.
	 * @param boolean $return Whether the output should be displayed immediately, or returned to be used elsewhere.
	 * @return string|null If the content is rendered, this will return null.
	 */
	private static function sendOutput(string $output, bool $return) : ?string
	{
		if($return)
		{
			return $output;
		}

		echo $output;

		return null;
	}
}