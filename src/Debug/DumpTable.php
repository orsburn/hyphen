<?php

namespace Hyphen\Debug;

trait DumpTable
{
	/** @var int */ private static $cliTableMaxWidth = 21; // The maximum any column is allowed to be, not the max width for the table.

	/**
	 * Dump an array as a table.
	 *
	 * From time to time, it may be convenient to show a uniform two-dimensional array as a table.  This is especially true if the array is actually used as a table.  Calling this dump function instead of Dump::dump() will transform the array into a grid view.
	 */
	public static function dumpTable(array $check, bool $return = false, bool $formatted = true) : ?string
	{
		if(!is_array($check))
		{
			return self::dumpError("The data is not something that can be shown as a table.", $return);
		}

		foreach($check as $row)
		{
			if(!is_array($row))
			{
				return self::dumpError("The data is not something that can be shown as a table.", $return);
			}

			break;
		}

		return PHP_SAPI != "cli"
			? self::sendOutput(self::htmlTable($check), $return)
			: self::sendOutput(self::cliTable($check, $formatted), $return);
	}

	/**
	 * Dump an array as an HTML table.
	 */
	private static function htmlTable(array $check) : string
	{
		$dumpId        = self::dumpId();
		$rowNumber     = 0;
		$rowHeaders    = self::rowHeaders($check);
		$columnHeaders = self::columnHeaders($check);

		$html  = "<!-- Table dump identified by $dumpId ........................... -->\n";
		$html .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-table.css") . "\n</style>\n\n";

		$html .= "<table class=\"table\" id=\"t_$dumpId\">\n";
		$html .= "	<thead class=\"tableHead\" id=\"t_$dumpId-head\">\n";
		$html .= "		<tr class=\"tableHeadMain\">\n";
		$html .= "			<th id=\"t_$dumpId-main\" colspan=\"" . (count($columnHeaders) + 1) . "\">table (" . count($check) . " x " . count($columnHeaders ?? []) . ")</th>\n";
		$html .= "		</tr>\n";
		$html .= "		<tr class=\"tableHeadColumnNames\">\n";
		$html .= "			<th class=\"columnNames\"></th>\n";

		foreach($columnHeaders as $columnHeader)
		{
			$html .= "			<th class=\"columnNames\">$columnHeader</th>\n";
		}

		$html .= "		</tr>\n";
		$html .= "	</thead>\n";
		$html .= "	<tbody id=\"t_$dumpId-body\">\n";

		$js  = "<script>\n";
		$js .= "var a_showHideElement_$dumpId = " . file_get_contents(__DIR__ . "/support/dump-element.js") . "\n\n";
		$js .= "var a_showHideBody_$dumpId = " . file_get_contents(__DIR__ . "/support/dump-body.js") . "\n\n";

		foreach($rowHeaders as $row)
		{
			$columnNumber = 0;

			$html .= "		<tr id=\"t_$dumpId" . "_$rowNumber\">\n";
			$html .= "			<th id=\"t_$dumpId" . "_$rowNumber\">$row</th>\n";

			foreach($check[$row] as $column)
			{
				$html .= "			<td id=\"t_$dumpId" . "_$rowNumber-$columnNumber\">$column</td>\n";

				$columnNumber ++;
			}

			$html .= "		<tr>\n";

			$rowNumber ++;
		}

		$output  = "$html\t</tbody>\n";
		$output .= "</table>";
		$output .= "\n\n";
		$output .= "$js</script>";
		$output .= "\n<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump an array as a table to the CLI.
	 */
	private static function cliTable(array $check, ?bool $formatted = true) : string
	{
		$rowHeaders    = self::rowHeaders($check);
		$columnHeaders = self::columnHeaders($check);
		$columnWidths  = array_fill(0, count($columnHeaders) + 1, 0);

		array_unshift($columnHeaders, null);  // Making room for the row header column.

		// Going through each row ($rowHeader) to find the widest column. --------------
		foreach($rowHeaders as $rowHeader)
		{
			$columnWidthsPosition = 1;  // Position 0 is for the row header itself.  This is determined separately since it's not really a row value.

			foreach($check[$rowHeader] as $cell)
			{
				$columnWidths[0] = min(             // On each iteration of the header row [rows], this is
					self::$cliTableMaxWidth,        // looking to see if the width of the first column, the
					max(                            // row number column, needs to be adjusted.
						$columnWidths[0],           //
						strlen((string) $rowHeader) //
					)                               //
				);                                  //

				if(gettype($cell) == "boolean")                                                                      // Getting the width of the current cell value, including a check for whether
				{                                                                                                    // or not the value is a boolean.  true would be 4 characters and false would
					$checkWidth = $cell ? 4 : 5;                                                                     // be 5.
				}                                                                                                    //
				else                                                                                                 //
				{                                                                                                    //
					$checkWidth = min(self::$cliTableMaxWidth, strlen((string) $cell));                              //
				}                                                                                                    //
				                                                                                                     //
				$columnWidth = min(self::$cliTableMaxWidth, $columnWidths[$columnWidthsPosition]);                   // The current width of the header.
				$headerWidth = min(self::$cliTableMaxWidth, strlen((string) $columnHeaders[$columnWidthsPosition])); // The header label may actually be longer than a cell value.

				// The column width should be the widest of, the current value, the current
				// column, or the width of the column header.
				$columnWidths[$columnWidthsPosition ++] = max($checkWidth, $columnWidth, $headerWidth);
			}
		}

		// ------------------------------------------------------------ Building headers
		$columnHeadersOutput = "";

		for($i = 0; $i < count($columnHeaders); $i ++)
		{
			$columnHeadersOutput .= " " . str_pad($columnHeaders[$i], $columnWidths[$i], " ", STR_PAD_BOTH) . " |";
		}

		$horizontalLine = self::cliHorizontal($columnWidths);

		// --------------------------------------- Building-up the table metadata header
		$output  = "table (" . count($rowHeaders) . " × " . (count($columnHeaders) - 1) . ")" . PHP_EOL;
		$output .= self::$cliIndent . $horizontalLine;
		$output .= self::$cliIndent . rtrim($columnHeadersOutput, "|") . PHP_EOL;
		$output .= self::$cliIndent . $horizontalLine;

		// ----------------------------------------------------------- Looping over rows
		foreach($rowHeaders as $rowHeader)
		{
			$columnWidthsPosition = 0;

			$rowOutput = self::$cliIndent . " " . str_pad($rowHeader, $columnWidths[$columnWidthsPosition ++], " ", STR_PAD_LEFT) . " |";

			// -------------------------------------------------------- Looping over columns
			foreach($check[$rowHeader] as $value)
			{
				// ------------------------------------------------------------ We have a string
				if(is_string($value))
				{
					$ellipsisOffset = 0; // This accommodates the multi-byte width of an ellipsis.

					// Making sure the column value isn't wider than the max.  This is putting an
					// ellipsis in the middle of the string.
					if(strlen($value) > self::$cliTableMaxWidth)
					{
						$shortedLeft  = ceil((self::$cliTableMaxWidth - 1) / 2);
						$shortedRight = floor((self::$cliTableMaxWidth - 1) / 2);
						$value        = rtrim(substr($value, 0, $shortedLeft)) ."…" . ltrim(substr($value, -$shortedRight));
						$ellipsisOffset = 2;
					}

					$rowOutput .= " " . str_pad($value, $columnWidths[$columnWidthsPosition] + $ellipsisOffset, " ") . " |";
				}
				// ----------------------------------------------------------- We have a boolean
				elseif(is_bool($value))
				{
					$rowOutput .= " " . str_pad($value ? "true" : "false", $columnWidths[$columnWidthsPosition], " ", STR_PAD_BOTH) . " |";
				}
				// --------------------------------------------------------------- It's a number
				else
				{
					$rowOutput .= " " . str_pad($value, $columnWidths[$columnWidthsPosition], " ", STR_PAD_LEFT) . " |";
				}

				$columnWidthsPosition ++;
			}

			$output .= rtrim($rowOutput, "|") . PHP_EOL;
		}

		return $output;
	}

	private static function rowHeaders($table)
	{
		return array_keys($table ?? []);
	}

	private static function columnHeaders($table)
	{
		$numberOfColumns = 0;
		$columns         = [];

		foreach($table as $row)
		{
			if(count($row) > $numberOfColumns)
			{
				$numberOfColumns = count($row);
				$columns         = array_keys($row);
			}
		}

		return $columns;
	}


	private static function cliHorizontal($columnWidths)
	{
		$template = "--------------------------------------------------";
		$output   = "";

		foreach($columnWidths as $width)
		{
			$output .= substr($template, 0, $width + 2) . "+";  // Adding two to account for the column padding.
		}

		return rtrim($output, "+") . PHP_EOL;
	}
}