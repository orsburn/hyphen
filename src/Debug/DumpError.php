<?php

namespace Hyphen\Debug;

trait DumpError
{
	/**
	 * Dump an error message.
	 *
	 * @param string $message
	 * @param bool $formatted
	 * @return string
	 */
	public static function dumpError(string $message, bool $return = false, bool $formatted = true) : ?string
	{
		return PHP_SAPI != "cli"
			? self::sendOutput(self::htmlError($message), $return)
			: self::sendOutput(self::cliError($message, $formatted), $return);
	}

	/**
	 * Dump an error as HTML.
	 *
	 * @param string $message
	 * @return string
	 */
	private static function htmlError(string $message) : string
	{
		$dumpId = self::dumpId();

		$output  = "<!-- Error dump identified by -->\n";
		$output .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-error.css") . "\n</style>\n\n";
		$output .= "<table class=\"error\" id=\"error_$dumpId\"><tbody><tr><th class=\"type\">error</th><td>$message</td></tr></tbody></table>\n";
		$output .= "<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump an error to the CLI.
	 *
	 * @param string $check
	 * @param boolean $formatted
	 * @return string
	 */
	private static function cliError(string $check, bool $formatted) : string
	{
		// TODO  Output with console colors.
		$output = "error $check";

		return $output;
	}
}