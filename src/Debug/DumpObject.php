<?php

namespace Hyphen\Debug;

trait DumpObject
{
	/**
	 * Dump an object as HTML.
	 *
	 * @param object $check
	 * @return string
	 */
	private static function htmlObject($check) : string
	{
		if(gettype($check) != "object")
		{
			throw new \InvalidArgumentException("Was expecting an object, but got \"" . gettype($check) . "\" instead,");
		}

		$dumpId        = self::dumpId();
		$elementNumber = 0;

		$html  = "<!-- Object dump identified by $dumpId ........................... -->\n";
		$html .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-object.css") . "\n</style>\n\n";

		$html .= "<table class=\"object\" id=\"a_$dumpId\">\n";
		$html .= "	<thead class=\"objectHead\" id=\"a_$dumpId-head\">\n";
		$html .= "		<tr>\n";
		$html .= "			<th id=\"a_$dumpId\" colspan=\"2\">" . get_class($check) . "</th>\n";
		$html .= "		</tr>\n";
		$html .= "	</thead>\n";
		$html .= "	<tbody id=\"a_$dumpId-body\">\n";

		$js  = "<script>\n";
		$js .= "var a_showHideElement_$dumpId = " . file_get_contents(__DIR__ . "/support/dump-element.js") . "\n\n";
		$js .= "var a_showHideBody_$dumpId = " . file_get_contents(__DIR__ . "/support/dump-body.js") . "\n\n";

		$js .= "document.querySelector('#a_$dumpId-head').addEventListener('click', a_showHideBody_$dumpId);\n\n";

		foreach($check as $key => $value)
		{
			ob_start();                         // Buffering for whatever the type is for the current
			self::dump($value);                 // value.
			$currentValue = ob_get_contents();  //
			ob_end_clean();                     //

			$html .= "		<tr id=\"a_$dumpId" . "_$elementNumber\">\n";
			$html .= "			<th id=\"a_$dumpId" . "_$elementNumber-key\">$key</th>\n";
			$html .= "			<td id=\"a_$dumpId" . "_$elementNumber-value\">$currentValue</td>\n";
			$html .= "		<tr>\n";

			$js .= "document.querySelector('#a_$dumpId" . "_$elementNumber-key').addEventListener('click', a_showHideElement_$dumpId);\n";

			$elementNumber ++;
		}

		$output  = "$html\t</tbody>\n";
		$output .= "</table>";
		$output .= "\n\n";
		$output .= "$js</script>";
		$output .= "\n<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump an object to the CLI.
	 *
	 * @param object $check
	 * @param bool $formatted
	 * @return string
	 */
	private static function cliObject($check, bool $formatted) : string
	{
		if(gettype($check) != "object")
		{
			throw new \InvalidArgumentException("Was expecting an object, but got \"" . gettype($check) . "\" instead,");
		}

		// BUG  This needs a bit more attention as nested objects are not displayed properly.  The array class might be the same way.  (Scott Orsburn | scott@orsburn.net | 11/16/2018)
		$keys = array_keys(get_object_vars($check));

		$keyLength = array_reduce(
					$keys,
					function($carry, $current){
						return max($carry, strlen((string) $current));
					},
					0);

		$output = get_class($check) . " (" . count($keys) . ")" . PHP_EOL;

		foreach($check as $key => $value)
		{
			$output .= self::$cliIndent . str_pad($key, $keyLength, " ", STR_PAD_LEFT) . " | " . self::dump($value, true) . PHP_EOL;
		}

		return $output;
	}
}