<?php

namespace Hyphen\Debug;

trait DumpFloat
{
	/**
	 * Dump a float as HTML.
	 *
	 * @param float $check
	 * @return string
	 */
	private static function htmlFloat(float $check) : string
	{
		$dumpId          = self::dumpId();
		$decimalLocation = strpos($check, ".");

		$output  = "<!-- Float dump identified by $dumpId -->\n";
		$output .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-float.css") . "\n</style>\n\n";
		$output .= "<table class=\"float\" id=\"float_$dumpId\"><tbody><tr><th class=\"type\">float</th><td>";
		$output .= substr($check, 0, $decimalLocation) . substr($check, $decimalLocation);
		$output .= "</td></tr></tbody></table>\n";
		$output .= "<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump a float to the CLI.
	 *
	 * @param float $check
	 * @param bool $formatted
	 * @return string
	 */
	private static function cliFloat(float $check, bool $formatted) : string
	{
		$decimalLocation = strpos($check, ".");

		$value = substr($check, 0, $decimalLocation) . substr($check, $decimalLocation);

		$output =
			  $formatted
			? CliColors::typeFloat("float") . " " . CliColors::float($value)
			: "float $value";

		return $output;
	}
}