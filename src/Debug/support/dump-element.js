showHideElement = function(){
	var e = document.querySelector('#' + this.id.replace(/\-key$/, '-value'));

	if(e.style.display != 'none'){
		e.style.display = 'none';
	}
	else{
		e.style.display = 'table-cell';
	}
};