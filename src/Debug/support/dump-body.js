showHideBody = function() /* This code is arranged specifically because it's included inline. */
{
	var e = document.querySelector("#" + this.id.replace(/\-head$/, "-body"));

	if(e.style.display != "none")
	{
		e.style.display = "none";
	}
	else
	{
		e.style.display = "table-row-group";
	}
};