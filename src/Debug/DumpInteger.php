<?php

namespace Hyphen\Debug;

trait DumpInteger
{
	/**
	 * Dump an integer as HTML.
	 *
	 * @param int $check
	 * @return string
	 */
	private static function htmlInteger(int $check) : string
	{
		$dumpId = self::dumpId();

		$output  = "<!-- Integer dump identified by -->\n";
		$output .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-integer.css") . "\n</style>\n\n";
		$output .= "<table class=\"integer\" id=\"integer_$dumpId\"><tbody><tr><th class=\"type\">integer</th><td>$check</td></tr></tbody></table>\n";
		$output .= "<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump an integer to the CLI.
	 *
	 * @param int $check
	 * @param bool $formatted
	 * @return string
	 */
	private static function cliInteger(int $check, bool $formatted) : string
	{
		$output
			= $formatted
			? CliColors::typeInt("int") . " " . CliColors::int($check)
			: "int $check";

		return $output;
	}
}