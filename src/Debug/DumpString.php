<?php

namespace Hyphen\Debug;

trait DumpString
{
	/**
	 * Dump a string as HTML.
	 *
	 * @param string $check
	 * @return string
	 */
	private static function htmlString(string $check) : string
	{
		$dumpId = self::dumpId();

		$output  = "<!-- String dump identified by -->\n";
		$output .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-string.css") . "\n</style>\n\n";
		$output .= "<table class=\"string\" id=\"string_$dumpId\"><tbody><tr><th class=\"type\">string (" . strlen($check) . ")</th><td>$check</td></tr></tbody></table>\n";
		$output .= "<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump a string to the CLI.
	 *
	 * @param string $check
	 * @param bool $formatted
	 * @return string
	 */
	private static function cliString(string $check, bool $formatted) : string
	{
		$output
			= $formatted
			? CliColors::typeString("string")
			: "string";
		$output .= " (" . strlen($check) . ") ";
		$output
			.= $formatted
			 ? CliColors::string($check)
			 : $check;

		return $output;
	}
}