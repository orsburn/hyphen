<?php

namespace Hyphen\Debug;

trait DumpNull
{
	/**
	 * Dump an NULL as HTML.
	 *
	 * @return string
	 */
	private static function htmlNull() : string
	{
		$dumpId = self::dumpId();

		$output  = "<!-- NULL dump identified by -->\n";
		$output .= "<style>\n" . file_get_contents(__DIR__ . "/support/dump-null.css") . "\n</style>\n\n";
		$output .= "<table class=\"null\" id=\"null_$dumpId\"><tbody><tr><th class=\"type\">&nbsp;&mdash;&mdash;&nbsp;</th><td>NULL</td></tr></tbody></table>\n";
		$output .= "<!-- End of $dumpId ........................................................ -->\n";

		return $output;
	}

	/**
	 * Dump an NULL to the CLI.
	 *
	 * @param bool $formatted
	 * @return string
	 */
	private static function cliNull(bool $formatted) : string
	{
		$output
			= $formatted
			? CliColors::typeNull(" —— ") . " " . CliColors::null("NULL")
			: "NULL";

		return $output;
	}
}