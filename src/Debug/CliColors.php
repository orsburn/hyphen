<?php

namespace Hyphen\Debug;

/*
This Wikipedia page is helpful.

https://en.wikipedia.org/wiki/ANSI_escape_code
*/

class CliColors
{
	/** @var string      */ private static $mode = "dark";
	/** @var object|null */ private static $styles;

	public static function arrayIndex(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->arrayIndex->foreground ?? ""),
			self::scrub(self::$styles->values->arrayIndex->background ?? "")
		);
	}

	public static function arrayKey(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->arrayKey->foreground ?? ""),
			self::scrub(self::$styles->values->arrayKey->background ?? "")
		);
	}

	public static function bool(string $value) : string
	{
		self::loadStyles();

		return self::render(
			$value,
			self::scrub(self::$styles->values->bool->foreground ?? ""),
			self::scrub(self::$styles->values->bool->background ?? "")
		);
	}

	public static function callable(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->callable->foreground ?? ""),
			self::scrub(self::$styles->values->callable->background ?? "")
		);
	}

	public static function float(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->float->foreground ?? ""),
			self::scrub(self::$styles->values->float->background ?? "")
		);
	}

	public static function int(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->int->foreground ?? ""),
			self::scrub(self::$styles->values->int->background ?? "")
		);
	}

	public static function null(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->null->foreground ?? ""),
			self::scrub(self::$styles->values->null->background ?? "")
		);
	}

	public static function property(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->property->foreground ?? ""),
			self::scrub(self::$styles->values->property->background ?? "")
		);
	}

	public static function string(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->values->string->foreground ?? ""),
			self::scrub(self::$styles->values->string->background ?? "")
		);
	}

	public static function typeArray(string $value) : string
	{
		return self::render(
			$value,
			self::scrub(self::$styles->types->array->foreground ?? ""),
			self::scrub(self::$styles->types->array->background ?? "")
		);
	}

	public static function typeBool(string $value) : string
	{
		self::loadStyles();

		return self::render(
			$value,
			self::scrub(self::$styles->types->bool->foreground ?? ""),
			self::scrub(self::$styles->types->bool->background ?? "")
		);
	}

	public static function typeCallable(string $value) : string
	{
		self::loadStyles();

		return self::render(
			$value,
			self::scrub(self::$styles->types->callable->foreground ?? ""),
			self::scrub(self::$styles->types->callable->background ?? "")
		);
	}

	public static function typeFloat(string $value) : string
	{
		self::loadStyles();

		return self::render(
			$value,
			self::scrub(self::$styles->types->float->foreground ?? ""),
			self::scrub(self::$styles->types->float->background ?? "")
		);
	}

	public static function typeInt(string $value) : string
	{
		self::loadStyles();

		return self::render(
			$value,
			self::scrub(self::$styles->types->int->foreground ?? ""),
			self::scrub(self::$styles->types->int->background ?? "")
		);
	}

	public static function typeNull(string $value) : string
	{
		self::loadStyles();

		return self::render(
			$value,
			self::scrub(self::$styles->types->null->foreground ?? ""),
			self::scrub(self::$styles->types->null->background ?? "")
		);
	}

	public static function typeString(string $value) : string
	{
		self::loadStyles();

		return self::render(
			$value,
			self::scrub(self::$styles->types->string->foreground ?? ""),
			self::scrub(self::$styles->types->string->background ?? "")
		);
	}

	/**
	 * Change the color mode to one of "dark" or "light."
	 *
	 * @param string $mode Either one of "dark" or "light."
	 */
	public static function mode(string $mode = "dark") : string
	{
		self::$mode = $mode;
		self::loadStyles();

		return $mode;
	}

	private static function render(string $value, ?string $foreground = null, ?string $background = null) : string
	{
		$colored  = "\033[48;";                        // Background
		$colored .=                                    //
			  (isset($background) && $background != "" //
			? "2;$background"                          //
			: "") . "m";                               //
		$colored .= "\033[38;";                        // Foreground
		$colored .=                                    //
			  (isset($foreground) && $foreground != "" //
			? "2;$foreground"                          //
			: "") . "m";                               //
		$colored .= $value;
		$colored .= "\033[m";

		return $colored;
	}

	/**
	 * Loads the XML file that defines the colors for CLI output.
	 *
	 * The colors are available at:
	 *
	 *   * self::$styles->types->string->foreground
	 *   * self::$styles->types->string->background
	 *
	 * @return void
	 */
	private static function loadStyles() : void
	{
		if(is_null(self::$styles))
		{
			self::$styles = simplexml_load_file(__DIR__ . "/support/dump-cli.xml");

			if(self::$mode == "dark")
			{
				self::$styles = self::$styles->dark;
			}
			elseif(self::$mode == "light")
			{
				self::$styles = self::$styles->light;
			}
		}
	}

	/**
	 * Replace the comma in separated list of red, green, blue (RGB) color values with a semi-colon.
	 *
	 * @param object|string $color The comma-separated list of colors.
	 * 
	 * @return string
	 */
	private static function scrub($color) : string
	{
		if(is_object($color))
		{
			return str_replace(" ", "", str_replace("," , ";", $color->__toString()));
		}

		return str_replace(" ", "", str_replace("," , ";", $color));
	}
}