<?php

namespace Hyphen\Logger;

/**
 * Class Rotator
 *
 * Provides a means to rotate a log file with a few parameters.
 *
 * @package Hyphen\Logger
 */
class Rotator
{
	/** @var string archive     */ public static $archive     = __DIR__ . "/../../logArchive";
	/** @var string interval    */ public static $interval    = "daily";  // daily | weekly | monthly | yearly | hourly
	/** @var integer|null limit */ public static $limit;                  // In megabytes.  It is superseded by interval if interval is present and not null.
	/** @var int $permissions   */ public static $permissions = 0644;     // rw-r--r--

	public static function rotate(string $logFile) : bool
	{
		$logFile = str_replace("\\", "/", $logFile);
		$statusContent = file_get_contents(self::$archive . "/.status");

		$info
			= $statusContent !== false
			? json_decode($statusContent)
			: null;

		$info = new \stdClass();
		$info->timestamp = $info->timestamp ?? date("m/d/Y h:i:s A T e \U\T\C O");
		$info->interval  = $info->interval ?? self::$interval ?? "daily";
		$info->limit     = $info->limit ?? self::$limit ?? null;

		if(!self::qualifies($logFile, $info))
		{
			return false;
		}

		$subDirectory
			= count(func_get_args()) > 1
			? "/" . ltrim(func_get_arg(0), "/\\")
			: "/";

		if(!file_exists(self::$archive))
		{
			try
			{
				mkdir(self::$archive, self::$permissions);
			}
			catch(\Exception $createException)
			{
				echo "The archive location didn't exist, so an attempt to create it was made.  It looks like the directory couldn't be created." . PHP_EOL;
				echo "  " . $createException->getMessage();

				return false;
			}
		}

		if(!is_writable(self::$archive))
		{
			throw new \Error(__METHOD__ . "():  The archive directory was unwritable.");
		}

		return self::doRotate($logFile, $subDirectory, $info);
	}

	private static function doRotate(string $logFile, string $subDirectory, object $info) : bool
	{
		rename($logFile, self::$archive . $subDirectory . basename($logFile) . "-" . date("Ymd"));
		touch($logFile);
		chmod($logFile, self::$permissions);
		self::updateInfo($info);

		return true;
	}

	/**
	 * If the interval or limit has been reached, then the log qualifies to be rotated.
	 *
	 * There is no guarantee that a copy will be made for each interval, just that there won't be a copy more often.
	 *
	 * It should be noted that the check is loose, and that there isn't a significant degree of checking for things like leap year and other date-related nuances.
	 *
	 * @return bool
	 */
	private static function qualifies(string $logFile, object $info) : bool
	{
		// We need a timestamp to know if another log should be written.
		if(!isset($info->timestamp))
		{
			return true;
		}

		if($logFileUnixTimestamp = date_create_from_format("m/d/Y h:i:s A T e \U\T\C O", $info->timestamp))
		{
			$logFileUnixTimestamp = $logFileUnixTimestamp->getTimestamp();
		}

		$nowUnixTimestamp  = time();
		$unixTimestampDiff = $nowUnixTimestamp - $logFileUnixTimestamp;

		/*
		FIXME: PHPStan doesn't really behave well here.  So ignore the error, and don't try the ?? operator.  PHPStan
		       doesn't know what to do with it in this context.
		*/
		// $intervalCheck = isset($info->interval) ? $info->interval : self::$interval;
		$intervalCheck = "daily";

		switch(strtolower($intervalCheck))
		{
			case "hourly":                         // These checks are based on a period from the last check.  For example,
				if($unixTimestampDiff > 3600)      // the daily interval will be 24 hours from the last check.  So, if the
				return true;                       // last check happened on 01/17/2018 10:28 AM, then the next rotation
			case "daily":                          // would be 01/18/2018 10.28 AM.  It would not be at midnight on 01/18.
				if($unixTimestampDiff > 86400)     //
				return true;                       // A future version of this might actually compare the values of day,
			case "weekly":                         // week, year, and so on.  Right now, the comparison is based on the
				if($unixTimestampDiff > 604800)    // last check, considering the distance of the interval from that time.
				return true;                       //
			case "monthly":                        //
				if($unixTimestampDiff > 18144000)  //
				return true;                       //
			case "yearly":                         //
				if($unixTimestampDiff > 31536000)  //
				return true;                       //
		}

		if(self::$limit && (filesize($logFile) >= self::$limit * 1024))  // Times 1024 in order to compare bytes to bytes.
		{
			return true;
		}

		return false;
	}

	private static function updateInfo(object $info) : bool
	{
		file_put_contents(self::$archive . "/.status", json_encode($info, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

		return true;
	}
}