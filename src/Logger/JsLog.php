<?php

namespace Hyphen\Logger;

class JsLog
{
	public static function log(...$message) : void
	{
		echo self::doLog("log", $message);
	}

	public static function table(...$message) : void
	{
		echo self::doLog("table", $message);
	}

	public static function warn(...$message) : void
	{
		echo self::doLog("warn", $message);
	}

	private static function doLog(string $type, ...$message) : string
	{
		$logOutput = "<script>console.$type(";

		foreach($message as $output)
		{
			if(is_string($output))
			{
				$logOutput .= "\"$output\", ";
			}
			else
			{
				$logOutput .= json_encode($output) . ", ";
			}
		}

		return rtrim($logOutput, ", ") . ");</script>\n";
	}
}