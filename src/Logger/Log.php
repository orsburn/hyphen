<?php

namespace Hyphen\Logger;

/*
It might just make the most sense to implement a PSR-3 version of a logger.  Or,
perhaps this class could just be a factory class that creates PSR-3 Log objects.

If the route of simply refactoring this class to match the PSR-3 interface, then
the context parameter would likely be the best [only] place to indicate a log
file location.

This might be a string pointing to a path, or a constant of some sort if the
message is supposed to go to stderr, for example.

	$context = ["destination"=>"/path/to/log/file.log"];
	$context = ["destination"=>HYPHEN::STDERR];
*/

define("HYPHEN_LOG_INFO", 0);     // Also available as Hyphen\Log::*
define("HYPHEN_LOG_MESSAGE", 0);  //
define("HYPHEN_LOG_WARN", 1);     //
define("HYPHEN_LOG_WARNING", 1);  //
define("HYPHEN_LOG_FATAL", 2);    //
define("HYPHEN_LOG_DEBUG", 3);    //

class Log
{
	const INFO    = HYPHEN_LOG_INFO;     // Also available as HYPHEN_LOG_*
	const MESSAGE = HYPHEN_LOG_MESSAGE;  //
	const WARN    = HYPHEN_LOG_WARN;     //
	const WARNING = HYPHEN_LOG_WARNING;  //
	const FATAL   = HYPHEN_LOG_FATAL;    //
	const DEBUG   = HYPHEN_LOG_DEBUG;    //

	/** @var string|null $logFile */ public $logFile;

	public function __construct(string $logFile = null)
	{
		$this->logFile = $logFile;
	}

	public function log(string $message, int $level = self::MESSAGE) : string
	{
		if(!$this->logFile)
		{
			throw new \Exception("No log file was specified.");
		}

		$level
			= func_num_args() === 2
			? func_get_arg(0)
			: null;

		$message = func_get_arg(count(func_get_args()) - 1);

		switch(true)
		{
			case $level === self::MESSAGE: $levelMessage = "Message:  "; break;
			case $level === self::WARNING: $levelMessage = "Warning:  "; break;
			case $level === self::FATAL: $levelMessage = "FATAL:  "; break;
			case $level === self::DEBUG: $levelMessage = "DEBUG:  "; break;
			default: $levelMessage = "";
		}

		file_put_contents(
			$this->logFile,
			date("m/d/Y h:i:s A T e \U\T\C O") . " " . trim($levelMessage . $message) . PHP_EOL,
			FILE_APPEND);

		return $message;
	}
}