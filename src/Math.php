<?php

namespace Hyphen;

use ErrorException;

/*
TODO:  More work may be necessary to improve accuracy based on the manual.

       https://www.php.net/manual/en/language.types.float.php
*/

class Math
{
	/**
	 * Tallies the sum for any of the following invocations.
	 *
	 * * add([1, 2, 3])
	 * * add("1, 2, 3") -- If passed as a string, the numbers will be considered floats.
	 * * add(1, 2, 3)
	 *
	 * @param mixed ...$numbers
	 *
	 * @return int|float|null
	 */
	public static function add(...$numbers) : mixed
	{
		$numbers = self::prepareNumbers($numbers);
		$tally = array_shift($numbers); // FIXME: Look into why array_shift() may return a null and perhaps deal with that situation.

		while($next = array_shift($numbers))
		{
			$tally += $next;
		}

		// if(is_int($tally))
		// {
		// 	return (int) $tally;
		// }

		return $tally;
	}

	/**
	 * Determines the difference for any of the following invocations.
	 *
	 * * difference([1, 2, 3]) | Consist of diffing from 1 parameter, an array or a string.
	 * * difference("1, 2, 3") |
	 * * difference(1, 2, 3)
	 *
	 * @param mixed ...$numbers
	 *
	 * @return int|float|null
	 */
	public static function subtract(...$numbers) : mixed
	{
		$numbers = self::prepareNumbers($numbers);
		$tally = array_shift($numbers); // FIXME: Look into why array_shift() may return a null and perhaps deal with that situation.

		while($next = array_shift($numbers))
		{
			$tally -= $next;
		}

		return $tally;
	}

	/**
	 * Determines the multiplicative of any of the following invocations.
	 *
	 * * multiply([1, 2, 3]) | Consist of multiplying from 1 parameter, an array or a string.
	 * * multiply("1, 2, 3") |
	 * * multiply(1, 2, 3)
	 *
	 * @param mixed ...$numbers
	 *
	 * @return int|float|null
	 */
	public static function multiply(...$numbers) : mixed
	{
		$numbers = self::prepareNumbers($numbers);
		$tally = array_shift($numbers); // FIXME: Look into why array_shift() may return a null and perhaps deal with that situation.

		while($next = array_shift($numbers))
		{
			$tally *= $next;
		}

		return $tally;
	}

	/**
	 * Determines the division of any of the following invocations.
	 *
	 * * divide([1, 2, 3]) | Consist of dividing from 1 parameter, an array or a string.
	 * * divide("1, 2, 3") |
	 * * divide(1, 2, 3)
	 *
	 * @param mixed ...$numbers
	 *
	 * @return int|float|null
	 */
	public static function divide(...$numbers) : mixed
	{
		$numbers = self::prepareNumbers($numbers);
		$tally = array_shift($numbers); // FIXME: Look into why array_shift() may return a null and perhaps deal with that situation.

		while(!is_null($next = array_shift($numbers)))
		{
			try
			{
				1 / $next;
			}
			catch(\DivisionByZeroError $e)
			{
				echo "Fatal error in call to " . __METHOD__ . "():  "  . $e->getMessage() . PHP_EOL;
				echo "    At least one parameter in the divide method was zero." . PHP_EOL;
				echo "    In file:     " . $e->getTrace()[0]["file"] . PHP_EOL;
				echo "    One line:    " . $e->getTrace()[0]["line"] . PHP_EOL;
				echo "    Parameters:  " . implode(", ", $e->getTrace()[0]["args"]) . PHP_EOL;

				die;
			}

			$tally /= $next;
		}

		return $tally;
	}

	/**
	 * The square value of a number.
	 *
	 * @param int|float $base
	 */
	public static function squared($base) : int|float|null
	{
		return pow($base, 2);
	}

	/**
	 * The cube value of a number.
	 *
	 * @param int|float $base
	 */
	public static function cubed($base) : int|float|null
	{
		return pow($base, 3);
	}

	/**
	 * Calculate the square root of a number.
	 *
	 * In essence, this is an alias of PHP's sqrt() function.
	 *
	 * @param int|float $base
	 */
	public static function squareRoot($base) : int|float|null
	{
		return sqrt($base);
	}

	/**
	 * Calculate the cube root of a number.
	 *
	 * @param int|float $base
	 */
	public static function cubeRoot($base) : int|float|null
	{
		return pow($base, 1/3);
	}

	/**
	 * The arithmetic functions allow arguments to be one of a comma-separated string of numbers, an array of numbers, or a series of numeric parameters.  This function checks for those possibilities and makes an array out of the arguments so that they can be worked with.
	 *
	 * @param mixed $numbers
	 *
	 * @return array<int|float>
	 */
	private static function prepareNumbers(mixed $numbers) : array
	{
		// More than 1 argument means it's the multiple parameters usage.
		if(func_num_args() > 1)
		{
			$numbers = func_get_args();
		}
		// If there's only 1 element, assume it's the string usage.
		elseif(
			is_array($numbers) &&
			(count($numbers) === 1) &&
			is_string($numbers[0])
		)
		{
			$numbers = explode(",", str_replace(" ", "", $numbers[0]));
		}
		else
		{
			$numbers = $numbers[0];
		}

		$preparedNumbers = [];

		// By this point, the numbers are in an array, or they were that way to begin with.
		foreach($numbers as $number)
		{
			if(is_int($number))
			{
				$preparedNumbers[] = (int) $number;
			}
			else
			{
				$preparedNumbers[] = (float) $number;
			}
		}

		return $preparedNumbers;
	}
}