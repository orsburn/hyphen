<?php

namespace Hyphen\Types;

class Type
{
	/**
	 * Converts a variable number of string arguments into an object with enumerated field values.
	 *
	 * @return Hyphen\Types\Pe
	 */
	public static function enum(string ...$fields) // FIXME: Would like to set a return type on this definition.
	{
		return (new \ReflectionClass("Hyphen\\Types\\Pe"))->newInstanceArgs($fields);
	}

	/**
	 * Converts a variable number of string arguments into an object with enumerated field values.
	 *
	 * @return Hyphen\Types\Pe
	 */
	public static function pe(string ...$fields) // FIXME: Would like to set a return type on this definition.
	{
		return (new \ReflectionClass("Hyphen\\Types\\Pe"))->newInstanceArgs($fields);
	}

	/**
	 * Returns the number of instances of a class found in the global scope.
	 *
	 * Be careful not to misinterpret the case where 0 instances might be found.  It might not always be appropriate to consider 0 as false.
	 */
	public static function exists(string $type) : int
	{
		$variables = array_keys($GLOBALS);
		$instances = 0;

		foreach($variables as $variable)
		{
			if($GLOBALS[$variable] instanceof $type)
			{
				$instances ++;
			}
		}

		return $instances;
	}

	/**
	 * @param mixed|mixed[]|null ...$fields
	 *
	 * @return Hyphen\Types\Po
	 */
	public static function po(...$fields) // FIXME: Would like to set a return type on this definition.
	{
		return (new \ReflectionClass("Hyphen\\Types\\Po"))->newInstanceArgs($fields);
	}
}