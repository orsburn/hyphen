<?php

namespace Hyphen\Types;

/**
 * Pseudo Object class
 *
 * This class creates what appears to be, and tries to act like a stdClass() instance with one difference.  Fields can be populated on instantiation.
 *
 * Usage:
 *
 *	$sample = new Po(["one"=>"number 1", "two"=>"number 2", "three"=>"number 3"]);
 *	$sample = new Po("one", "number 1", "two", "number 2", "three", "number 3");
 *	$sample = new Po(["one", "two", "three"]);
 *	$sample = new Po();
 *
 * The preferred method is the first example, whereby fields are created via an associative array.  The array keys become the instance fields, and the values, obviously, the values.
 *
 * An alternative manner of populating fields is with a list of arguments.  If counting arguments starting with 1, all odd numbered arguments become fields, and an the even numbered argument immediately following each odd argument is that odd argument's value.
 *
 * The third option is to simply pass an array of string values.  These values will be used as field names, and their values will be initialized to null.
 *
 * A fourth option is available too.  This one allows the instance to be created with no fields.  This will create an empty Po object.
 */
class Po
{
	/**
	 * Usage:
	 *
	 *	$sample = new Po(["one"=>"number 1", "two"=>"number 2", "three"=>"number 3"]);
	 *	$sample = new Po("one", "number 1", "two", "number 2", "three", "number 3");
	 *	$sample = new Po(["one", "two", "three"]);
	 *	$sample = new Po();
	 *
	 * @param mixed|mixed[]|null ...$fields
	 */
	public function __construct(...$fields)
	{
		if(count($fields) > 1)
		{
			$this->populateFromArguments($fields);
		}
		elseif((count($fields) === 1) && !is_null($fields[0]))
		{
			$this->populateFromArray($fields[0]);
		}
	}

	/**
	 * @param string $closure
	 * @param array<array> $args
	 * @return mixed
	 */
	public function __call(string $closure, array $args)
	{
		if(is_callable($this->$closure))
		{
			return call_user_func_array($this->$closure->bindTo($this), $args);
		}

		return new \Exception("The method does not exist on this instance of Po.");
	}

	/**
	 * Undocumented function
	 *
	 * @param array<array> $fields
	 * @return void
	 */
	private function populateFromArray(array $fields) : void
	{
		$keys = array_keys($fields);

		// This loop will probably fall out after only an iteration or two.
		foreach($keys as $key)
		{
			// If any key is an integer, the whole array will be assumed to be an indexed array.

			if($isIndexedArray = is_int($key))
			{
				continue;
			}
		}

		if($isIndexedArray ?? false)
		{
			foreach($fields as $field)
			{
				$this->$field = null;
			}
		}
		else
		{
			foreach($fields as $field => $value)
			{
				$this->$field = $value;
			}
		}
	}

	/**
	 * @param array<array> $fields
	 * @return void
	 */
	private function populateFromArguments(array $fields) : void
	{
		for($i = 0; $i < count($fields); $i += 2)
		{
			$this->{$fields[$i]} = $fields[$i + 1] ?? null;
		}
	}
}