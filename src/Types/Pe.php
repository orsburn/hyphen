<?php

namespace Hyphen\Types;

/**
 * A pseudo Enumerable class
 *
 * A simple way to generate an enumerable-like structure.
 */
class Pe
{
	/** @var array<string>|null */ private ?array $fields = null;
	                               private int $index = 0;

	public function __construct(string ...$fields)
	{
		foreach($fields as $field)
		{
			$this->fields[] = $field;
			$this->$field = ++ $this->index; // FIXME: Are dynamic properties going away?
		}
	}

	/**
	 * The number of fields in the pseudo enumerable instance.
	 */
	public function getElementCount() : int
	{
		return $this->index;
	}

	public function getField(string $field) : int
	{
		if(isset($this->$field))
		{
			return $this->$field;
		}

		throw new \Exception("Pe field \"$field\" does not exist.");
	}

	public function getFields() : array
	{
		return $this->fields;
	}

	public function getFieldByValue(int $value) : string
	{
		if($value > 0 && $value <= count($this->fields))
		{
			return $this->fields[$value - 1];
		}

		throw new \Exception("Pe field number $value does not exist");
	}
}