<?php

namespace Hyphen\Cli;

class Help
{
	/** var array $help */         public $help          = [];
	/** var int $indentWidth */    public $indentWidth   = 4;
	/** var int $contentMargin */  public $contentMargin = 2;
	/** var int $outputWidth */    public $outputWidth   = 80;
	/** var int $argumentsWidth */ private $argumentsWidth;

	public function __construct(array $help = [])
	{
		$this->help = $help;
	}


	public function help() : string
	{
		$output = "";

		$this->argumentsWidth = array_reduce($this->help, function($previous, $current){
			$arguments     = implode(", ", $current["arguments"]);
			$currentLength = strlen($arguments);

			if($currentLength > $previous)
			{
				return $currentLength;
			}

			return $previous;
		}, 0);

		// Rendering help for a single argument.
		if(func_num_args() === 1)
		{
			foreach($this->help as $key => $entry)
			{
				if(array_search(trim(func_get_arg(0)), $entry["arguments"]) !== false)
				{
					$output .= $this->render($key);
				}
			}
		}
		// Otherwise, display all help topics.
		else
		{
			$helpKeys = array_keys($this->help);

			foreach($helpKeys as $key)
			{
				$output .= $this->render($key) . "\n\n";
			}
		}

		return $output;
	}


	private function render($key)
	{
		$arguments = implode(", ", $this->help[$key]["arguments"]);

		$entry = str_pad("", $this->indentWidth, " ", STR_PAD_LEFT) . str_pad($arguments, $this->argumentsWidth, " ");

		// 1.  Wrapping the help content to the width of the display less any margins and justification.
		$content = wordwrap($this->help[$key]["content"], $this->outputWidth - ($this->argumentsWidth + $this->indentWidth + $this->contentMargin));  // Include a 4-character indent and a 2-character margin to the content.
		// 2.  Breaking the help content back into individual lines.
		$content = explode("\n", $content);
		// 3.  Re-assembling the help content lines with the necessary padding and justification.
		$content = implode("\n" . str_pad("", ($this->argumentsWidth + $this->indentWidth + $this->contentMargin), " ", STR_PAD_LEFT), $content);

		$entry .= str_pad("", $this->contentMargin, " ") . $content;

		return $entry;
	}
}