<?php

namespace Hyphen;

class AlphaNumeric
{
	/**
	 * Convert an integer in to a sequence of letters as though counting with letters.  The pattern is such:
	 *
	 *  1 == A
	 *  2 == B
	 *    …
	 * 26 == Z
	 * 27 == AA
	 * 28 == AB
	 *    …
	 * 52 == AZ
	 * 53 == BA
	 *
	 * @param int $number
	 * 
	 * @return string
	 */
	static public function convertToLetters(int $number) : string
	{
		$index = 0;       // Used to know when to bail out of the function.

		$ref = [chr(64)]; // Setting this to lower than A because of how the
                          // current letter is checked in the recursion below.

		$cursor = 0;      // Thinking of the column reference as an array, this
                          // is the current position the reference letter is
                          // being determined for.

		while($index < $number)
		{
			if(ord($ref[$cursor]) < 90)                              // Updating the reference at the cursor position, if possible.
			{                                                        //
				$ref[$cursor] = chr(ord($ref[$cursor]) + 1);         //
				$index ++;                                           // Advancing the index counter any time the reference is updated.
			}                                                        //
			else                                                     // If the letter at the current position can't be updated, then we
			{                                                        // look left to see if /that/ position can be updated.
				$cursor --;                                          // <----'
                                                                     //
				if(isset($ref[$cursor]))                             // If the position left exists, then we reset the position we just
				{                                                    // had back to "A."
					$ref[$cursor + 1] = "A";                         // <------------'
                                                                     //
					if(ord($ref[$cursor]) < 90)                      // Then, check if the new position (position left) can be increased.
					{                                                // If it can be, then the cursor is moved back to the end of the
						$ref[$cursor] = chr(ord($ref[$cursor]) + 1); // reference, and the process is started all over again.  If the
						$cursor = count($ref) - 1;                   // current position is not updated, then the cursor stays where it
						$index ++;                                   // is and the recursion started back at the top.  It will inherently
					}                                                // fail the update this time, and so the cursor will be moved left
				}                                                    // again.  If there is no position left, then the reference needs to
				else                                                 // be expanded
				{                                                    //
					$ref = array_fill(0, count($ref) + 1, "A");      //
					$cursor = count($ref) - 1;                       //
					$index ++;                                       //
				}                                                    //
			}                                                        //
		}

		return implode("", $ref);
	}

	/**
	 * Convert a series of letters to their integer equivalent.  For instance, The Dth item is the 4th item.
	 *
	 * @param string $letters
	 *
	 * @return int
	 */
	static public function convertToNumber(string $letters) : int
	{
		return 0;
	}
}