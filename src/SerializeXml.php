<?php

namespace Hyphen;

use TypeError;

class SerializeXml
{
	/**
	 * Serialize an item as an XML formatted document.  It will include the XML header at the top of the document.
	 *
	 * If there should be a root element for the rendered XML, then the array should be assigned to a single element associative array.
	 *
	 * ```toXml(["rootNode"=>["one"=>"uno", "two"=>"dos", "three"=>"tres"]]);```
	 *
	 * Optional items are sent for the second argument as an associative array.
	 *
	 *     *  "version"=>"1.0"
	 *     *  "encoding=>"encoding"
	 *     *  "standalone=>"standalone"
	 *     *  "indent"=>2
	 *     *  "nodeName"=>"node"
	 *     *  "arrayElementsName"=>"element"
	 *     *  "scalarNodesName"=>"value"
	 *     *  "nullNodesName"=>"value"
	 *
	 * @param object|array<mixed> $source This can be an array or an object, or a combination of these.
	 * @param array<string|int> $options This is an array of optional details that can be sent to the XML rendering process.  An XML version is sent by default.  See the list of options above.
	 *
	 * @see toXml()
	 */
	public static function toXmlDocument(mixed $source, array $options = ["version"=>"1.0"]) : string
	{
		$declaration = "<?xml version=\"{$options["version"]}\"";

		if(isset($options["encoding"]))
		{
			$declaration .= " encoding=\"{$options["encoding"]}\"";
		}

		if(isset($options["standalone"]))
		{
			$declaration .= " standalone=\"{$options["standalone"]}\"";
		}

		$declaration .= "?>";

		return $declaration . PHP_EOL . self::toXml($source, $options);
	}

	/**
	 * Serialize an item as an XML formatted structure.
	 *
	 * If there should be a root element for the rendered XML, then the array should be assigned to a single element associative array.
	 *
	 * ```toXml(["rootNode"=>["one"=>"uno", "two"=>"dos", "three"=>"tres"]]);```
	 *
	 * Optional items are sent for the second argument as an associative array.
	 *
	 *     *  "version"=>"1.0"
	 *     *  "encoding=>"encoding"
	 *     *  "standalone=>"standalone"
	 *     *  "indent"=>2
	 *     *  "nodeName"=>"node"
	 *     *  "arrayElementsName"=>"element"
	 *     *  "scalarNodesName"=>"value"
	 *     *  "nullNodesName"=>"value"
	 *
	 * @param object|array<mixed> $source This can be an array or an object, or a combination of these.
	 * @param array<string|int> $options This is an array of optional details that can be sent to the XML rendering process.  See the list of options above.
	 *
	 * @see toXmlDocument()
	 */
	public static function toXml($source, array $options = []) : string
	{
		if(is_array($source))
		{
			return self::arrayToXml($source, $options);
		}
		elseif(is_object($source))
		{
			return self::objectToXml($source, $options);
		}
		elseif(is_null($source))
		{
			return self::nullToXml($options);
		}

		return self::scalarToXml($source, $options);
	}

	/**
	 * Serialize an array as an XML formatted structure.
	 *
	 * If there should be a root element for the rendered XML, then the array should be assigned to a single element associative array.
	 *
	 * ```toXml(["rootNode"=>["one"=>"uno", "two"=>"dos", "three"=>"tres"]]);```
	 *
	 * Optional items are sent for the second argument as an associative array.
	 *
	 *     *  "version"=>"1.0"
	 *     *  "encoding=>"encoding"
	 *     *  "standalone=>"standalone"
	 *     *  "indent"=>2
	 *     *  "nodeName"=>"node"
	 *     *  "arrayElementsName"=>"element"
	 *     *  "scalarNodesName"=>"value"
	 *     *  "nullNodesName"=>"value"
	 *
	 * @param array<mixed> $array This is the array to be serialized.
	 * @param array<string|int> $options This is an array of optional details that can be sent to the XML rendering process.  See the list of options above.
	 */
	public static function arrayToXml(array $array, array $options = []) : string
	{
		/*
		This is the real workhorse of this class.  Most structures will be converted to
		arrays and then passed to this method to render the XML.  This is what happens
		with SerializeXml::objectToXml().

		An exception to this, is if SerializeXml::toXml() is called with a scalar value.
		*/
		$nodeName = $options["arrayElementsName"] ?? ($options["nodeName"] ?? "element");

		$indentLevel  = self::getIndentLevel($options);

		$element = "";

		foreach($array as $node => $value)
		{
			// Avoiding outputting indexed arrays with numeric nodes.
			$node = is_numeric($node) ? $nodeName : $node;

			// The opening tag will always be at the same place, regardless of whether or not the value is a scalar or a structure.
			$element .= str_pad("<$node>", strlen("<$node>") + $indentLevel, "\t", STR_PAD_LEFT);

			if(is_scalar($value))
			{
				$element .= "$value</$node>" . PHP_EOL; // Close tag here to keep it on the same line.
			}
			elseif(is_null($value))
			{
				$element .= "null</$node>" . PHP_EOL; // Close tag here to keep it on the same line.
			}
			else
			{
				$options["indent"] = $indentLevel + 1;

				$element .= PHP_EOL; // Need a new line to render the next array.
				$element .= self::toXml($value, $options);
				$element .= PHP_EOL;

				// Need to close the tag for structures here, so it can be indented properly.
				$element .= str_pad("</$node>", strlen("</$node>") + $indentLevel, "\t", STR_PAD_LEFT) . PHP_EOL;
			}
		}

		return rtrim($element);
	}

	/**
	 * Serialize an object as an XML formatted structure.
	 *
	 * If there should be a root element for the rendered XML, then the array should be assigned to a single element associative array.
	 *
	 * ```toXml(["rootNode"=>["one"=>"uno", "two"=>"dos", "three"=>"tres"]]);```
	 *
	 * Optional items are sent for the second argument as an associative array.
	 *
	 *     *  "version"=>"1.0"
	 *     *  "encoding=>"encoding"
	 *     *  "standalone=>"standalone"
	 *     *  "indent"=>2
	 *     *  "nodeName"=>"node"
	 *     *  "arrayElementsName"=>"element"
	 *     *  "scalarNodesName"=>"value"
	 *     *  "nullNodesName"=>"value"
	 *
	 * @param object $object This is the object to be serialized.
	 * @param array<string|int> $options This is an array of optional details that can be sent to the XML rendering process.  See the list of options above.
	 */
	public static function objectToXml(object $object, array $options = []) : string
	{
		$options["indent"] = self::getIndentLevel($options);

		return self::arrayToXml((array) $object, $options);
	}

	/**
	 * Will convert a non-complex data type to an XML node.
	 *
	 * Boolean values will be preserved such that the XML will render with a true or false value instead of a 1 (true) or empty (false).
	 *
	 * @param mixed $scalar This is the variable to be serialized.
	 * @param array<string|int> $options This is an array of optional details that can be sent to the XML rendering process.  See the list of options above.
	 */
	public static function scalarToXml($scalar, array $options = []) : string
	{
		$nodeName = $options["scalarNodesName"] ?? ($options["nodeName"] ?? "value");
		$indentLevel = self::getIndentLevel($options);
		$output = "";

		// This check is here to handle cases when a non-scalar value is passed
		// if(is_array($scalar) || is_object($scalar) || is_null($scalar))
		if(!is_scalar($scalar))
		{
			throw new TypeError("Expected a scalar value, but a non-scalar value (" . gettype($scalar) . ") was provided.");
		}
		elseif(is_bool($scalar))
		{
			$scalar
				= $scalar === true
				? "true"
				: "false";

			$nodeContent = "<$nodeName>$scalar</$nodeName>";
			$output = str_pad($nodeContent, strlen($nodeContent) + $indentLevel, "\t", STR_PAD_LEFT);
		}
		elseif(is_numeric($scalar))
		{
			$nodeContent = "<$nodeName>$scalar</$nodeName>";
			$output = str_pad($nodeContent, strlen($nodeContent) + $indentLevel, "\t", STR_PAD_LEFT);
		}
		elseif(is_string($scalar))
		{
			$scalar = htmlspecialchars($scalar, ENT_XML1 | ENT_QUOTES);
			$nodeContent = "<$nodeName>$scalar</$nodeName>";
			$output .= str_pad($nodeContent, strlen($nodeContent) + $indentLevel, "\t", STR_PAD_LEFT);
		}

		return $output . PHP_EOL;
	}

	public static function nullToXml(array $options = []) : string
	{
		$nodeName = $options["nullNodesName"] ?? ($options["nodeName"] ?? "value");
		$indentLevel = self::getIndentLevel($options);
		$output = "<$nodeName>NULL</$nodeName>";

		return str_pad($output, strlen($output) + $indentLevel, "\t", STR_PAD_LEFT) . PHP_EOL;
	}


	/**
	 * @param array<string|int> $options
	 *
	 * @return integer
	 */
	private static function getIndentLevel(array $options = []) : int
	{
		return isset($options["indent"]) ? (int) $options["indent"] : 0;
	}
}