<?php

namespace Hyphen;

define("HYPHEN_MATCH_LOOSE", 1);                   // 0000001  Ignores case and type
define("HYPHEN_MATCH_CASE", 2);                    // 0000010  Case sensitive
define("HYPHEN_MATCH_STRICT", 4);                  // 0000100  Matches type
define("HYPHEN_MATCH_PARTIAL", 8);                 // 0001000  Partial matches are allowed
define("HYPHEN_MATCH_EXCLUDE", 16);                // 0010000  Matches everything but the value being matched against
define("HYPHEN_MATCH_EXPRESSION", 32);             // 0100000  Matches against an expression rather than a specific value
define("HYPHEN_MATCH_BOOLEAN", 64);                // 1000000  Matches as booleans
define("HYPHEN_MATCH_BOOL", HYPHEN_MATCH_BOOLEAN); //

define("HYPHEN_SORT_REGULAR", SORT_REGULAR);
define("HYPHEN_SORT_NUMERIC", SORT_NUMERIC);
define("HYPHEN_SORT_STRING", SORT_STRING);
define("HYPHEN_SORT_LOCALE_STRING", SORT_LOCALE_STRING);
define("HYPHEN_SORT_NATURAL", SORT_NATURAL);
define("HYPHEN_SORT_FLAG_CASE", SORT_FLAG_CASE);
define("HYPHEN_SORT_UP", 0);
define("HYPHEN_SORT_DOWN", 1);

class Arrays
{
	const MATCH_LOOSE      = HYPHEN_MATCH_LOOSE;      // 0000001  Ignores case and type
	const MATCH_CASE       = HYPHEN_MATCH_CASE;       // 0000010  Case sensitive
	const MATCH_STRICT     = HYPHEN_MATCH_STRICT;     // 0000100  Matches type
	const MATCH_PARTIAL    = HYPHEN_MATCH_PARTIAL;    // 0001000  Partial matches are allowed
	const MATCH_EXCLUDE    = HYPHEN_MATCH_EXCLUDE;    // 0010000  Matches everything but the value being matched against
	const MATCH_EXPRESSION = HYPHEN_MATCH_EXPRESSION; // 0100000  Matches against an expression rather than a specific value
	const MATCH_BOOLEAN    = HYPHEN_MATCH_BOOLEAN;    // 1000000  Matches as booleans
	const MATCH_BOOL       = HYPHEN_MATCH_BOOL;       //

	const SORT_REGULAR       = SORT_REGULAR;
	const SORT_NUMERIC       = SORT_NUMERIC;
	const SORT_STRING        = SORT_STRING;
	const SORT_LOCALE_STRING = SORT_LOCALE_STRING;
	const SORT_NATURAL       = SORT_NATURAL;
	const SORT_FLAG_CASE     = SORT_FLAG_CASE;
	const SORT_UP            = 0;
	const SORT_DOWN          = 1;

	/**
	 * Sort a grid in the same way that the SPL asort() function does.
	 *
	 * @param array<mixed> $data
	 *
	 * @see https://www.php.net/manual/en/function.asort
	 */
	public static function asortGrid(array &$data, mixed $sortColumn, int $sortType = self::SORT_REGULAR) : bool
	{
		return self::sortGrid($data, $sortColumn, self::SORT_UP, $sortType);
	}

	/**
	 * Sort a grid in reverse order, in the same way that the SPL arsort() function does.
	 *
	 * @param array<mixed> $data
	 *
	 * @see https://www.php.net/manual/en/function.arsort.php
	 */
	public static function arsortGrid(array &$data, mixed $sortColumn, int $sortType = self::SORT_REGULAR) : bool
	{
		return self::sortGrid($data, $sortColumn, self::SORT_DOWN, $sortType);
	}

	/**
	 * @param array<mixed> $data
	 */
	public static function sortGrid(array &$data, mixed $sortColumn, int $sortDirection = self::SORT_UP, int $sortType = self::SORT_REGULAR) : bool
	{
		if(count($data) < 1)
		{
			return false;
		}

		$sortColumn                                          // Ensuring that the column being sorted on actually exists,
			= isset($data[0][$sortColumn])                   // otherwise try to sort on the first column.
			? array_column($data, $sortColumn)               //
			: array_column($data, array_keys($data[0])[0]);  //

		if($sortDirection === self::SORT_UP)
		{
			asort($sortColumn, $sortType);
		}
		elseif($sortDirection === self::SORT_DOWN)
		{
			arsort($sortColumn, $sortType);
		}

		$virtualData = $data;
		$reindexer = 0;

		foreach($sortColumn as $sortedPosition=>$sortedValue)
		{
			$data[$reindexer ++] = $virtualData[$sortedPosition];
		}

		return true;
	}

	/**
	 * Provides a way to take an array and spread the entries across a number of other arrays.  This is a convenient way to represent array values in a columnar fashion.
	 *
	 * @param array<mixed> $array The array of values that will be broken down into columns.
	 * @param int $columnCount The number of columns the values should be spread across.  The default is 2.
	 *
	 * @return array<array<mixed>> A 2-dimensional array of values will be returned.  Each dimension holds the values for a "column."  The number of elements matches the number of columns specified in <strong>$columnCount</strong>.
	 */
	public static function spread(array $array, $columnCount = 2) : array
	{
		$entriesCount = count($array);
		$index = 0;
		$entriesPerColumn = 0;
		/*
		Indicates which "side" to carry the balance on.
		(balanceOn = 0 | 1  begin (the left) | end (the right))
		*/
		$balanceOn = 0;

		if($entriesCount < $columnCount)
		{
			$columnCount = $entriesCount;
		}

		$balance = $entriesCount % $columnCount;

		if($balance == 0)
		{
			$entriesPerColumn = ($entriesCount / $columnCount);
		}
		elseif($entriesCount < $columnCount)
		{
			$entriesPerColumn = 1;
			$balance = 0;
		}
		else
		{
			$entriesPerColumn = ($entriesCount - $balance) / $columnCount;
		}

		$currentColumn = 0;

		while($currentColumn < $columnCount)
		{
			$columnEntries = [];

			for($columnBoundary = 0; $columnBoundary < $entriesPerColumn; $columnBoundary ++)
			{
				$columnEntries[] = $array[$index ++];
			}

			if($balance > 0)
			{
				$columnEntries[] = $array[$index ++];
				$balance --;
			}

			$columns[$currentColumn ++] = $columnEntries;
		}

		return $columns ?? [];
	}

	/**
	 * As a way to extend the unset() construct, this function will search an array for a value and then unset() the array's element at the position the value was found at.
	 *
	 * The advantage to this routine is obviously that the position of the array element to unset doesn't need to be known.
	 *
	 * @param array<mixed> $array The target array to search for the value in.
	 * @param mixed $value The value to search the target array for.
	 *
	 * @return void Like the standard unset() construct, this routine returns nothing.
	 *
	 * @see unset()
	 */
	public static function unsetByValue(array &$array, $value) : void
	{
		$element = array_search($value, $array);

		if($element >= 0)
		{
			unset($array[$element]);
		}
	}

	/**
	 * Search through a multi-dimensional array as though it were a dataset and returns the "rows" that match a search term.
	 *
	 * Thinking of a 2-dimensional array as a data grid and using this function will allow a set of "records" to be returned by matching "column" values to a search value.
	 *
	 * There is no good way to search through a multi-dimensional array other than perhaps using the recursive functions like array_search() and array_filter().  The callback portion of these functions is what would be used to qualify an element as "found."  But there are a couple of issues though.  One, is that it's much  more clumsy.  The second, is that it's not as easy to accommodate the options that this function does.
	 *
	 * @param array<mixed> $array The array to search against.
	 * @param mixed $column The column to be searched against.
	 * @param mixed $find The value to search the 2-dimensional array for.  This includes expressions as well.  This is done by providing the "right-hand side" of the expression as a string.  For example, a search against an <strong>age</strong> column for ages greater than 25 would have a <strong>$value</strong> argument passed as the string "> 25."
	 * @param int $limiter (optional) Takes an optional fourth argument to indicate certain limits on the search.  Use the HYPHEN_MATCH_* constants, including bitwise "or-ing" them together to restrict the results.  For example:
	 *
	 * 		1. HYPHEN_MATCH_EXCLUDE will return everything but the value in the $find argument.  Searching for "new york" will match anything that is <u>not</u> "New York" or "new york."
	 * 		2. HYPHEN_MATCH_EXCLUDE | HYPHEN_MATCH_STRICT will return everything but the value in the $find argument while taking the case of the search value into consideration.  Searching for "New York" would exclude "New York" but would <em>include</em> "new york."
	 *
	 * @return array<mixed> Returns a 2-dimensional array from the larger, parent 2-dimensional array.
	 *
	 * @see array_filter()
	 * @see array_search()
	 */
	public static function find(array $array, $column, $find, int $limiter = self::MATCH_LOOSE)
	{
		$results = array();

		if(func_num_args() == 4)
		{
			/*
			Just checking to see if the only bit set was exclude.  If so, then we need to
			use loose as the default.
			*/
			if($limiter == self::MATCH_EXCLUDE)
			{
				$limiter = self::MATCH_EXCLUDE | self::MATCH_LOOSE;
			}
		}

		if(($limiter & HYPHEN_MATCH_EXCLUDE) == HYPHEN_MATCH_EXCLUDE) // Exclude
		{                                                             //
			$matchQualifier = false;                                  //
		}                                                             //
		else                                                          // Include
		{                                                             //
			$matchQualifier = true;                                   //
		}                                                             //

		// ---------------------------------------------------------------------- Strict
		if(($limiter & HYPHEN_MATCH_STRICT) == HYPHEN_MATCH_STRICT)
		{
			foreach($array as $row)
			{
				if(($row[$column] === $find) == $matchQualifier)
				{
					$results[] = $row;
				}
			}
		}
		// ---------------------------------------------------- Loose with partial match
		elseif(($limiter & HYPHEN_MATCH_PARTIAL) == HYPHEN_MATCH_PARTIAL)
		{
			foreach($array as $row)
			{
				if(stripos($row[$column], $find) !== $matchQualifier)
				{
					$results[] = $row;
				}
			}
		}
		// ----------------------------------------------------------------------- Loose
		elseif(($limiter & HYPHEN_MATCH_LOOSE) == HYPHEN_MATCH_LOOSE)
		{
			$valueAsString = strtolower($find);

			foreach($array as $row)
			{
				if(
					(
						($row[$column] == $find) ||
						(strtolower($row[$column]) == $valueAsString)
					) ==
					$matchQualifier
				)
				{
					$results[] = $row;
				}
			}
		}
		// ------------------------------------------- Case sensitive with partial match
		elseif(($limiter & (HYPHEN_MATCH_CASE ^ HYPHEN_MATCH_PARTIAL)) == (HYPHEN_MATCH_CASE ^ HYPHEN_MATCH_PARTIAL))
		{
			foreach($array as $row)
			{
				if(strpos($row[$column], $find) !== $matchQualifier)
				{
					$results[] = $row;
				}
			}
		}
		// -------------------------------------------------------------- Case sensitive
		elseif(($limiter & HYPHEN_MATCH_CASE) == HYPHEN_MATCH_CASE)
		{
			foreach($array as $row)
			{
				if($row[$column] == $find)
				{
					$results[] = $row;
				}
			}
		}
		// ------------------------------------------------------------------ Expression
		elseif(($limiter & HYPHEN_MATCH_EXPRESSION) == HYPHEN_MATCH_EXPRESSION)
		{
			foreach($array as $row)
			{
				if(is_string($row[$column]))
				{
					$check = "\"{$row[$column]}\"";
				}
				elseif(is_bool($row[$column]))
				{
					if(Core::considered($row[$column]))
					{
						$check = "true";
					}
					else
					{
						$check = "false";
					}
				}
				else
				{
					$check = $row[$column];
				}

				if(eval("if($check $find){return true;}else{return false;}"))
				{
					$results[] = $row;
				}
			}
		}
		// --------------------------------------------------------------------- Boolean
		elseif(($limiter & HYPHEN_MATCH_BOOLEAN) == HYPHEN_MATCH_BOOLEAN)
		{
			foreach($array as $row){if((Core::considered($row[$column]) == Core::considered($find)) == $matchQualifier){$results[] = $row;}}
		}

		return $results;
	}

	/**
	 * Casts all elements of an array to a desired type.
	 *
	 * @param array<mixed> $array The array that should have its elements cast.
	 * @param int|string $type It's intended to accept a Hyphen constant for data type, but it will also handle a string representation as well.  For example, if the type should be an integer, any of the following could be passed as the type argument:  HYPHEN_INTEGER; HYPHEN_INT; "integer"; "int."
	 */
	function cast(array &$array, $type) : bool
	{
		switch($type)
		{
			case HYPHEN_INTEGER:
			case HYPHEN_INT:
			case HYPHEN_BIT:
			case HYPHEN_BYTE:
			case "integer":
			case "int":
			case "byte":
			case "bit":
				array_walk($array, function(&$value, $key){$value = (int) $value;});
				return true;
			case HYPHEN_FLOAT:
			case HYPHEN_DOUBLE:
			case "float":
			case "double":
				array_walk($array, function(&$value, $key){$value = (float) $value;});
				return true;
			case HYPHEN_STRING:
			case HYPHEN_STR:
			case HYPHEN_CHARACTER:
			case HYPHEN_CHAR:
			case "string":
			case "str":
			case "character":
			case "char":
				array_walk($array, function(&$value, $key){$value = (string) $value;});
				return true;
		}

		return false;
	}
}