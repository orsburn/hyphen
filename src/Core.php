<?php

namespace Hyphen;

use InvalidArgumentException;
use Logger\Log;
use Psalm\Issue\InvalidArgument;

define("HYPHEN_INTEGER", 0);    // Integers
define("HYPHEN_INT", 0);        //
define("HYPHEN_BIT", 0);        //
define("HYPHEN_BYTE", 0);       //

define("HYPHEN_STRING", 1);     // Strings
define("HYPHEN_STR", 1);        //
define("HYPHEN_CHARACTER", 1);  //
define("HYPHEN_CHAR", 1);       //
define("HYPHEN_CHR", 1);        //

define("HYPHEN_FLOAT", 2);      // Floats
define("HYPHEN_DOUBLE", 2);     //

define(__NAMESPACE__ . "\\INTEGER", 0);    // Integers
define(__NAMESPACE__ . "\\INT", 0);        //
define(__NAMESPACE__ . "\\BIT", 0);        //
define(__NAMESPACE__ . "\\BYTE", 0);       //

define(__NAMESPACE__ . "\\STRING", 1);     // Strings
define(__NAMESPACE__ . "\\STR", 1);        //
define(__NAMESPACE__ . "\\CHARACTER", 1);  //
define(__NAMESPACE__ . "\\CHAR", 1);       //
define(__NAMESPACE__ . "\\CHR", 1);        //

define(__NAMESPACE__ . "\\FLOAT", 2);      // Floats
define(__NAMESPACE__ . "\\DOUBLE", 2);     //

if(\PHP_OS == "WINNT")
{
	require_once __DIR__ . "/Lib/Windows/unimplemented.php";
}

class Core
{
	use Welcome;

	/** @var string $root    */ public $root    = "";
	/** @var string $logFile */ public $logFile = "";

	public function __construct()
	{
		$this->root = self::root();
		$this->logFile = $this->root . "/application.log";
	}

	/**
	 * Looking for a file named index.php or hyphen.php at the base of the application folder.
	 *
	 * Calling this method or referencing the property should essentially be the same thing.
	 */
	public static function root() : string
	{
		// It's unorthodox, but it should be more efficient than using a loop and a series of variable assignments.
		$root = dirname(                              // /application                            5. The last check.
				dirname(                              // /application/vendor                     4.
				dirname(                              // /application/vendor/orsburn             3.
				dirname(                              // /application/vendor/orsburn/hyphen      2.
				str_replace("\\", "/", __DIR__)))));  // /application/vendor/orsburn/hyphen/src  1. Checking here first.


		// Look in some of the common places to make a valiant effort.  Otherwise, it just reports the application root.
		if(file_exists("$root/src/index.php") || file_exists("$root/src/hyphen.php"))
		{
			return "$root/src";
		}
		elseif(file_exists("$root/app/index.php") || file_exists("$root/app/hyphen.php"))
		{
			return "$root/app";
		}
		elseif(file_exists("$root/application/index.php") || file_exists("$root/application/hyphen.php"))
		{
			return "$root/application";
		}
		elseif(file_exists("$root/source/index.php") || file_exists("$root/source/hyphen.php"))
		{
			return "$root/source";
		}

		return $root;
	}

	/**
	 * Allows pages to be indicated in the URI.
	 *
	 * In environments that rewrite URIs, the trailing portion of the URI is typically considered application data and not actual path information.  This method allows file names to be indicated in the URL in a conventional manner and they will be loaded as expected.
	 *
	 * For example:  http://server.domain.tld/application/phpinfo.php will try to load a script named "phpinfo.php" if one exists.
	 *
	 * @param bool $allow This is optional and defaults to true. It's the flag that indicates whether the trailing part of a URI that might represent a file should be treated as a file.
	 */
	public static function allowPages(bool $allow = true) : void
	{
		// TODO:  This should optionally be allow an array of files it should not load if they're present (exceptions).  (Scott Orsburn | scott.orsburn@kaomso.com | 01/14/2017)
		$pathInfo   = filter_input(INPUT_SERVER, "PATH_INFO");
		$scriptPath = dirname(filter_input(INPUT_SERVER, "SCRIPT_FILENAME"));
		$checkFile  = $scriptPath . $pathInfo;

		if($allow && !is_dir($checkFile))
		{
			if(file_exists($checkFile))
			{
				include $checkFile;
			}
			else
			{
				http_response_code(404);
			}
		}
		else
		{
			http_response_code(200);
		}
	}

	/**
	 * A boolean will be returned based on the type of data and its value. Items that are not considered false, but aren't inherently true either will be considered true.  This is the in the C vein of, non-false values are true.
	 *
	 * These are considered true:
	 *
	 *   * true
	 *   * "true"
	 *   * 1
	 *   * "1"
	 *   * yes
	 *   * "Some random string"
	 *   * 12
	 *   * 12.1
	 *   * 847
	 *   * object
	 *   * array
	 *   * "null" (with argument flag = false (default))
	 *
	 * These are considered false:
	 *
	 *   * null
	 *   * false
	 *   * "false"
	 *   * 0
	 *   * 0.0
	 *   * "0"
	 *   * no
	 *   * ""
	 *   * " "
	 *   * "null" (with argument flag = true)
	 *
	 * @param mixed $check Non-scalar parameters, except for null, are not allowed.
	 */
	public static function considered(mixed $check, bool $nullAsString = false) : bool
	{
		if($nullAsString && is_string($check) && strtolower(trim($check)) == "null")
		{
			return false;
		}

		return match(true)
		{
			/* NULL   */ is_null($check) => false,
			/* 1      */ (is_int($check) || is_float($check)) && ($check < 0 || $check > 0) => true,
			/* 0      */ $check === 0 => false,
			/* object */ is_object($check) => true,
			/* array  */ is_array($check) => true,
			/* false  */ strtolower(trim($check)) == "false" => false,
			/* no     */ strtolower(trim($check)) == "no" => false,
			/* "0"    */ trim($check) === "0" => false,
			/* ""     */ trim($check) == "" => false,
			/* !""    */ $check != "" => true, // Accommodates "yes", "true", and "1"
			             default => false
		};
	}

	/**
	 * Checks for NULL, if the check is null, then the default value is returned.
	 *
	 * @param bool $nullAsString Forces the check to consider the string "null" as null.
	 */
	public static function default(mixed $check, mixed $value, bool $nullAsString = false) : mixed
	{
		if(
			is_null($check) ||
			($nullAsString && strtolower(trim($check)) == "null")
		)
		{
			 return $value;
		}

		return $check;
	}
}