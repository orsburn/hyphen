<?php

namespace Hyphen;

class System
{
	/**
	 * Traverse a directory path to collect all the entries along the way.
	 *
	 * @param string $path The fully-qualified path to seed the crawl from.
	 * @param boolean|null $silent Set this flag to suppress the output from rendering to standard output.
	 *
	 * @see array System::crawl() An alias for walk().
	 */
	public static function walk(string $path, ?bool $silent = false) : array
	{
		// TODO: This should have an option to render results as a stream in order to
		//       handle cases of very large collections of file paths.

		$path = rtrim($path, "/");
		$directories = [];
		$listing = [];
		$output = [];

		if($path && is_dir($path))
		{
			$directories[] = $path;
		}

		while(count($directories) > 0)
		{
			$directory = array_shift($directories);

			$listing
				= is_dir($directory)
				? scandir($directory)
				: [];

				array_shift($listing);  // Bye-bye .!
				array_shift($listing);  // Bye-bye ..!

			if($silent)
			{
				ob_start();
			}

			foreach($listing as $entry)
			{
				// TODO:  Somewhere in here there will need to be a check for things like
				//        shortcuts and link files.

				$path = "$directory/$entry";
				echo $path . PHP_EOL;
				$output[] = $path;

				if(is_dir($path))
				{
					$directories[] = $path; // This will need to be scanned too. Queueing it up!
				}
			}

			if($silent)
			{
				ob_end_clean();
			}
		}

		return $output;
	}

	/**
	 * Traverse a directory path to collect all the entries along the way.
	 *
	 * This is an alias for ```System::walk()```.
	 *
	 * @param string $path The fully-qualified path to seed the crawl from.
	 * @param boolean|null $silent Set this flag to suppress the output from rendering to standard output.
	 *
	 * @see array System::walk()
	 */
	public static function crawl(string $path, ?bool $silent = false) : array
	{
		return self::walk($path, $silent);
	}

	/**
	 * A wrapper for the unlink() built-in function.
	 *
	 * @param string $name The fully-qualified path to the file to remove.
	 * @param resource|null $context An optional context stream resource.
	 *
	 * @return boolean
	 */
	public static function rm(string $filename, $context = null) : bool
	{
		return unlink($filename, $context);
	}

	/**
	 * Delete a directory. Since directories must be empty before they can be removed, this recursively scans a directory tree, removing files.
	 *
	 * @param string $path The fully qualified directory path.
	 *
	 * @return boolean Returns ```false``` if the path is not a directory, or ```true``` otherwise.
	 */
	public static function rmdir(string $path) : bool
	{
		if(!is_dir($path))
		{
			return false;
		}

		$tree = self::walk($path, true);
		$numberOfEntries = count($tree);

		// Running through the listing to remove files.
		for($entryNumber = 0; $entryNumber < $numberOfEntries; $entryNumber ++)
		{
			if(is_file($tree[$entryNumber]))
			{
				unlink($tree[$entryNumber]);
				unset($tree[$entryNumber]);
			}
		}

		// Running through the listing again to actually remove the directories.
		for($entryNumber = 0; $entryNumber < $numberOfEntries; $entryNumber ++)
		{
			if(is_dir($tree[$entryNumber]))
			{
				rmdir($tree[$entryNumber]);
				unset($tree[$entryNumber]);
			}
		}

		return true;
	}
}