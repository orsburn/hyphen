# README

The last thing developers need is another framework regardless of the platform.  None-the-less, here it is.  It was originally created as an experiment and over the years grew out of philosophy, experimentation, need, or just because.

It is currently undergoing a massive re-write to break it out of a monolithic, all encompassing, end-all-be-all, and so on, of a project, to one that conforms to the current package-based, more singularly focused collection of libraries.


## What is it?

_The "incompleteness" of this project is appreciated.  It's here because there's at least some degree of usability in its current form._

The short of the library is that it serves as a route handler for inbound HTTP requests, and by extension will ultimately provide a way to send outbound HTTP requests too.

* Inbound via `Http\Response()`
* Outbound via `Http\Request()` (Unimplemented as of yet.)

***NOTE**:  While this is not PSR-7 compliant yet, it's still functional in this bare form.  At some point it will support the PSR-7 standard as well.*

In addition, it has become a collection of other utilities as well.  Things like a pseudo-object (Po) for easy instantiation of a data object as a stdClass() instead of an associative array.

***NOTE**:  This project is going to be broken out into a collection of smaller libraries that can be consumed individually.*

## Installation

The only supported method of installation is via Composer.

`composer require orsburn/hyphen`

Checkout the project [homepage](https://orsburn.bitbucket.io/hyphen/) for more.