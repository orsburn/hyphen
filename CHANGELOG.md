# Change Log

### [1.3.0] 2019-09-20

  * The optional $params argument should be an associative array of options now.
  * Improved the way the SerializeXml methods work.

### [1.1.5] 2019-02-28

  * Solidified the namespace structure some more.
  * Improved support for finding the application root.